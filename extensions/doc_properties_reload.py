#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
doc_properties_reload.py
Inkscape extension to reload and apply document properties

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=bad-whitespace
# pylint: disable=fixme
# pylint: disable=broad-except
# pylint: disable=no-member

import doc_properties
import inkex

from doc_properties import formatted
from doc_properties import inkbool2string
from doc_properties import get_color_from_int


# debug
def delNS(name):
    for tag, namespace in inkex.NSS.items():
        if namespace not in name:
            continue
        else:
            # FIXME: better substring extraction
            name = "%s:%s" % (tag, name[name.find("}")+1:])
    return name


def print_attribute_info(attributes):
    inkex.debug("Number of attributes: %s\n" % len(attributes.keys()))
    print_attributes(attributes)


def print_attributes(attributes):
    for name, value in sorted(attributes.items()):
        inkex.debug('%s = %r' % (delNS(name), value))
    inkex.debug("\n")


def compare_attributes(old, new):
    for name in sorted(old.keys()):
        inkex.debug("%s = %r --> %r" % (delNS(name), old.get(name), new.get(name)))


class DocProps(doc_properties.DocProperties):
    '''
    Force reload of document properties (<svg>, <sodipodi:namedview> attributes)
    '''
    def __init__(self):
        doc_properties.DocProperties.__init__(self)

        # Guides
        self.OptionParser.add_option("--nv_showguides",
                                     action="store", type="inkbool",
                                     dest="nv_showguides", default=True,
                                     help="Show Guides")
        # Grids
        self.OptionParser.add_option("--nv_showgrid",
                                     action="store", type="inkbool",
                                     dest="nv_showgrid", default=True,
                                     help="Show Grids")
        # Scope
        self.OptionParser.add_option("--nv_set_all",
                                     action="store", type="inkbool",
                                     dest="nv_set_all", default=False,
                                     help="Apply in all tabs")
        self.OptionParser.add_option("--nv_verbose",
                                     action="store", type="inkbool",
                                     dest="nv_verbose", default=False,
                                     help="Verbose mode")
        self.OptionParser.add_option("--nv_query_only",
                                     action="store", type="inkbool",
                                     dest="nv_query_only", default=False,
                                     help="Query only (don't modify)")


    def query_attributes(self, node):
        # unpack globals
        page_width, page_width_unit, page_height, page_height_unit = self.page_size
        vb_width, vb_height = self.view_box[2:4]
        x_scale, y_scale = self.aspect_ratio[2:4]
        # inkscape version (runtime)
        inkex.debug("Inkscape version:\t%s\n"
                    % self.ink_vers)
        # globals: SVG user units
        inkex.debug("SVG user unit (x-scale, in page unit): \t%s (%s)"
                    % (formatted(x_scale), page_width_unit))
        inkex.debug("SVG user unit (y-scale, in page unit): \t%s (%s)"
                    % (formatted(y_scale), page_height_unit))
        inkex.debug("SVG user unit (x-scale, in CSS Pixel): \t%s (px)"
                    % formatted(x_scale * self.uuconv[page_width_unit]))
        inkex.debug("SVG user unit (y-scale, in CSS Pixel): \t%s (px)\n"
                    % formatted(y_scale * self.uuconv[page_height_unit]))
        # globals: page size
        page_unit = self.page_unit
        inkex.debug("Page unit:   \t%s"
                    % page_unit)
        inkex.debug("Page width:  \t%s"
                    % formatted(page_width))
        inkex.debug("Page height: \t%s\n"
                    % formatted(page_height))
        # globals: viewBox
        inkex.debug("SVG viewBox width:   \t%s"
                    % formatted(vb_width))
        inkex.debug("SVG viewBox height:  \t%s\n"
                    % formatted(vb_height))
        inkex.debug("preserveAspectRatio: \t%s\n"
                    % self.rt.get('preserveAspectRatio', "xMidYMid meet"))
        # globals: scale factor
        inkex.debug("Document scale x:  \t1 : %s"
                    % formatted(1.0 / x_scale))
        inkex.debug("Document scale y:  \t1 : %s\n"
                    % formatted(1.0 / y_scale))
        # document display units
        inkex.debug("Document Display unit: %s\n"
                    % self.nv.get(inkex.addNS('document-units', 'inkscape')))
        # inkex
        doc_unit = self.getDocumentUnit()
        doc_width, doc_height = (self.unittouu(self.rt.get(dim)) for dim in ['width', 'height'])
        inkex.debug("Document unit (inkex):   \t%s"
                    % doc_unit)
        inkex.debug("Document width (inkex):  \t%s"
                    % formatted(doc_width))
        inkex.debug("Document height (inkex): \t%s\n"
                    % formatted(doc_height))

    def set_attributes_touch(self, node):
        # trigger reload by changing Id for <svg> root element
        self.rt.set('id', self.uniqueId("SVGRoot_"))

    def doit(self, node):
        if self.options.nv_query_only:
            self.query_attributes(node)
        else:
            self.set_attributes_touch(node)

    def set_attribs(self):

        global DEBUG
        DEBUG = self.options.nv_verbose

        nv = self.getNamedView()

        if DEBUG:
            nv_attributes_old = dict(nv.attrib)

        self.doit(nv)

        if DEBUG:
            nv_attributes_new = dict(nv.attrib)
            # compare_attributes(nv_attributes_old, nv_attributes_new)
            print_attributes(nv_attributes_old)
            print_attributes(nv_attributes_new)


if __name__ == '__main__':
    e = DocProps()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
