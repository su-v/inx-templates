#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
doc_properties_set.py
Inkscape extension to modify document properties

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=bad-whitespace
# pylint: disable=fixme
# pylint: disable=broad-except
# pylint: disable=no-member

import doc_properties
import inkex

from doc_properties import formatted
from doc_properties import inkbool2string
from doc_properties import get_color_from_int

try:
    inkex.localize()
except AttributeError:
    import gettext
    _ = gettext.gettext


DEBUG = 0


# debug
def delNS(name):
    for tag, namespace in inkex.NSS.items():
        if namespace not in name:
            continue
        else:
            # FIXME: better substring extraction
            name = "%s:%s" % (tag, name[name.find("}")+1:])
    return name


def print_attribute_info(attributes):
    inkex.debug("Number of attributes: %s\n" % len(attributes.keys()))
    print_attributes(attributes)


def print_attributes(attributes):
    for name, value in sorted(attributes.items()):
        inkex.debug('%s = %r' % (delNS(name), value))
    inkex.debug("\n")


def compare_attributes(old, new):
    for name in sorted(old.keys()):
        inkex.debug("%s = %r --> %r" % (delNS(name), old.get(name), new.get(name)))


class DocProps(doc_properties.DocProperties):
    '''
    Modify document properties (<svg>, <sodipodi:namedview> attributes)
    '''
    def __init__(self):
        doc_properties.DocProperties.__init__(self)

        # Id
        self.OptionParser.add_option("--nv_id",
                                     action="store", type="string",
                                     dest="nv_id", default="base",
                                     help="Id of namedview node")
        # Display unit
        self.OptionParser.add_option("--nv_display_unit_id",
                                     action="store", type="string",
                                     dest="nv_display_unit_id", default="mm",
                                     help="Document default unit identifier")
        # Page
        self.OptionParser.add_option("--nv_page_format",
                                     action="store", type="string",
                                     dest="nv_page_format", default="custom",
                                     help="Page format (presets)")
        self.OptionParser.add_option("--nv_page_orientation",
                                     action="store", type="string",
                                     dest="nv_page_orientation", default="portrait",
                                     help="Page orientation")
        self.OptionParser.add_option("--nv_page_unit_id",
                                     action="store", type="string",
                                     dest="nv_page_unit_id", default="mm",
                                     help="Page unit identifier")
        self.OptionParser.add_option("--page_width",
                                     action="store", type="float",
                                     dest="page_width", default=210.0,
                                     help="Page width")
        self.OptionParser.add_option("--page_height",
                                     action="store", type="float",
                                     dest="page_height", default=297.0,
                                     help="Page height")
        self.OptionParser.add_option("--svg_page_scale_factor",
                                     action="store", type="float",
                                     dest="svg_page_scale_factor", default=1.0,
                                     help="Page viewBox scale factor")
        # viewBox
        self.OptionParser.add_option("--svg_viewbox_scale",
                                     action="store", type="string",
                                     dest="svg_viewbox_scale", default="auto",
                                     help="Page viewBox scale mode")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_x",
                                     action="store", type="float",
                                     dest="svg_viewbox_scale_custom_x", default=1.0,
                                     help="Page viewBox custom scale x")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_y",
                                     action="store", type="float",
                                     dest="svg_viewbox_scale_custom_y", default=1.0,
                                     help="Page viewBox custom scale y")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_uniform",
                                     action="store", type="inkbool",
                                     dest="svg_viewbox_scale_custom_uniform", default=True,
                                     help="Page viewBox custom uniform scaling")
        self.OptionParser.add_option("--svg_viewbox_custom_string",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_string", default="0 0 100 100",
                                     help="Page viewBox custom string")
        self.OptionParser.add_option("--svg_viewbox_custom_aspect_align",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_aspect_align", default="none",
                                     help="Page viewBox custom AspectRatio")
        self.OptionParser.add_option("--svg_viewbox_custom_aspect_clip",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_aspect_clip", default="meet",
                                     help="Page viewBox custom meetOrSlice")
        self.OptionParser.add_option("--svg_viewbox_custom_rect",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_rect", default="none",
                                     help="Page viewBox custom rect")
        # Resize margins
        self.OptionParser.add_option("--nv_fit_margin_top",
                                     action="store", type="float",
                                     dest="nv_fit_margin_top", default=0,
                                     help="Margin top")
        self.OptionParser.add_option("--nv_fit_margin_left",
                                     action="store", type="float",
                                     dest="nv_fit_margin_left", default=0,
                                     help="Margin left")
        self.OptionParser.add_option("--nv_fit_margin_right",
                                     action="store", type="float",
                                     dest="nv_fit_margin_right", default=0,
                                     help="Margin right")
        self.OptionParser.add_option("--nv_fit_margin_bottom",
                                     action="store", type="float",
                                     dest="nv_fit_margin_bottom", default=0,
                                     help="Margin bottom")
        # Display
        self.OptionParser.add_option("--nv_pagecolor",
                                     action="store", type="string",
                                     dest="nv_pagecolor", default="#ffffff",
                                     help="Page color")
        self.OptionParser.add_option("--nv_pageopacity",
                                     action="store", type="float",
                                     dest="nv_pageopacity", default=0.0,
                                     help="Page opacity")
        self.OptionParser.add_option("--nv_pagecheckerboard",
                                     action="store", type="inkbool",
                                     dest="nv_pagecheckerboard", default=False,
                                     help="Page checkerboard background")
        self.OptionParser.add_option("--nv_showborder",
                                     action="store", type="inkbool",
                                     dest="nv_showborder", default=True,
                                     help="Show Page border")
        self.OptionParser.add_option("--nv_bordercolor",
                                     action="store", type="string",
                                     dest="nv_bordercolor", default="#ffffff",
                                     help="Border color")
        self.OptionParser.add_option("--nv_borderopacity",
                                     action="store", type="float",
                                     dest="nv_borderopacity", default=0.0,
                                     help="Border opacity")
        self.OptionParser.add_option("--nv_borderlayer",
                                     action="store", type="inkbool",
                                     dest="nv_borderlayer", default=False,
                                     help="Border on top of drawing")
        self.OptionParser.add_option("--nv_showpageshadow",
                                     action="store", type="inkbool",
                                     dest="nv_showpageshadow", default=True,
                                     help="Show page shadow")
        self.OptionParser.add_option("--nv_color_rendering",
                                     action="store", type="string",
                                     dest="nv_color_rendering", default="unset",
                                     help="Color rendering")
        self.OptionParser.add_option("--nv_shape_rendering",
                                     action="store", type="string",
                                     dest="nv_shape_rendering", default="unset",
                                     help="Shape rendering")
        self.OptionParser.add_option("--nv_text_rendering",
                                     action="store", type="string",
                                     dest="nv_text_rendering", default="unset",
                                     help="Text rendering")
        self.OptionParser.add_option("--nv_image_rendering",
                                     action="store", type="string",
                                     dest="nv_image_rendering", default="unset",
                                     help="Image rendering")
        # Rendering
        self.OptionParser.add_option("--nv_pageshadow",
                                     action="store", type="string",
                                     dest="nv_pageshadow", default="2",
                                     help="Page shadow type")
        # Window
        self.OptionParser.add_option("--nv_window_width",
                                     action="store", type="int",
                                     dest="nv_window_width", default=800,
                                     help="Window width")
        self.OptionParser.add_option("--nv_window_height",
                                     action="store", type="int",
                                     dest="nv_window_height", default=640,
                                     help="Window height")
        self.OptionParser.add_option("--nv_window_x",
                                     action="store", type="int",
                                     dest="nv_window_x", default=0,
                                     help="Window X")
        self.OptionParser.add_option("--nv_window_y",
                                     action="store", type="int",
                                     dest="nv_window_y", default=0,
                                     help="Window Y")
        self.OptionParser.add_option("--nv_window_maximized",
                                     action="store", type="inkbool",
                                     dest="nv_window_maximized", default=False,
                                     help="Maximized window")
        # Zoom
        self.OptionParser.add_option("--nv_zoom",
                                     action="store", type="float",
                                     dest="nv_zoom", default=0.35,
                                     help="Zoom level")
        self.OptionParser.add_option("--nv_cx",
                                     action="store", type="float",
                                     dest="nv_cx", default=0,
                                     help="View center x")
        self.OptionParser.add_option("--nv_cy",
                                     action="store", type="float",
                                     dest="nv_cy", default=0,
                                     help="View center y")
        # Guides
        self.OptionParser.add_option("--nv_showguides",
                                     action="store", type="inkbool",
                                     dest="nv_showguides", default=False,
                                     help="Show Guides")
        self.OptionParser.add_option("--nv_guidecolor",
                                     action="store", type="string",
                                     dest="nv_guidecolor", default="#0000ff",
                                     help="Guide color")
        self.OptionParser.add_option("--nv_guideopacity",
                                     action="store", type="float",
                                     dest="nv_guideopacity", default=0.4,
                                     help="Guide opacity")
        self.OptionParser.add_option("--nv_guidehicolor",
                                     action="store", type="string",
                                     dest="nv_guidehicolor", default="#ff0000",
                                     help="Guide hilight color")
        self.OptionParser.add_option("--nv_guidehiopacity",
                                     action="store", type="float",
                                     dest="nv_guidehiopacity", default=0.4,
                                     help="Guide hilight opacity")
        self.OptionParser.add_option("--nv_guides_around_page",
                                     action="store", type="inkbool",
                                     dest="nv_guides_around_page", default=False,
                                     help="Guides around page")
        # Grids
        self.OptionParser.add_option("--nv_showgrid",
                                     action="store", type="inkbool",
                                     dest="nv_showgrid", default=False,
                                     help="Show Grids")
        self.OptionParser.add_option("--nv_creategrid",
                                     action="store", type="inkbool",
                                     dest="nv_creategrid", default=False,
                                     help="Create Grid")
        # Grid presets
        self.OptionParser.add_option("--nv_grid_preset",
                                     action="store", type="string",
                                     dest="nv_grid_preset", default="default",
                                     help="Grid preset sizes")
        self.OptionParser.add_option("--nv_grid_preset_type",
                                     action="store", type="string",
                                     dest="nv_grid_preset_type", default="display",
                                     help="Grid preset type")
        # Grid type
        self.OptionParser.add_option("--nv_grid_rect_units",
                                     action="store", type="string",
                                     dest="nv_grid_rect_units", default="display",
                                     help="Rectangular grid units")
        self.OptionParser.add_option("--nv_grid_rect_originx",
                                     action="store", type="float",
                                     dest="nv_grid_rect_originx", default=0.0,
                                     help="Rectangular grid origin X")
        self.OptionParser.add_option("--nv_grid_rect_originy",
                                     action="store", type="float",
                                     dest="nv_grid_rect_originy", default=0.0,
                                     help="Rectangular grid origin Y")
        self.OptionParser.add_option("--nv_grid_rect_spacingx",
                                     action="store", type="float",
                                     dest="nv_grid_rect_spacingx", default=0.0,
                                     help="Rectangular grid spacing X")
        self.OptionParser.add_option("--nv_grid_rect_spacingy",
                                     action="store", type="float",
                                     dest="nv_grid_rect_spacingy", default=0.0,
                                     help="Rectangular grid spacing Y")
        self.OptionParser.add_option("--nv_grid_axo_units",
                                     action="store", type="string",
                                     dest="nv_grid_axo_units", default="display",
                                     help="Axonometric grid units")
        self.OptionParser.add_option("--nv_grid_axo_originx",
                                     action="store", type="float",
                                     dest="nv_grid_axo_originx", default=0.0,
                                     help="Axonometric grid origin X")
        self.OptionParser.add_option("--nv_grid_axo_originy",
                                     action="store", type="float",
                                     dest="nv_grid_axo_originy", default=0.0,
                                     help="Axonometric grid origin Y")
        self.OptionParser.add_option("--nv_grid_axo_spacingy",
                                     action="store", type="float",
                                     dest="nv_grid_axo_spacingy", default=0.0,
                                     help="Axonometric grid spacing Y")
        self.OptionParser.add_option("--nv_grid_axo_anglex",
                                     action="store", type="float",
                                     dest="nv_grid_axo_anglex", default=30.0,
                                     help="Axonometric angle X")
        self.OptionParser.add_option("--nv_grid_axo_angley",
                                     action="store", type="float",
                                     dest="nv_grid_axo_angley", default=30.0,
                                     help="Axonometric angle Y")
        # Grid options
        self.OptionParser.add_option("--nv_grid_enabled",
                                     action="store", type="inkbool",
                                     dest="nv_grid_enabled", default=True,
                                     help="Grid enabled")
        self.OptionParser.add_option("--nv_grid_visible",
                                     action="store", type="inkbool",
                                     dest="nv_grid_visible", default=True,
                                     help="Grid visible")
        self.OptionParser.add_option("--nv_grid_snapvisiblegridlinesonly",
                                     action="store", type="inkbool",
                                     dest="nv_grid_snapvisiblegridlinesonly", default=True,
                                     help="Snap to visible grid lines only")
        self.OptionParser.add_option("--nv_grid_empspacing",
                                     action="store", type="int",
                                     dest="nv_grid_empspacing", default=5,
                                     help="Major grid line every")
        self.OptionParser.add_option("--nv_grid_dotted",
                                     action="store", type="inkbool",
                                     dest="nv_grid_dotted", default=False,
                                     help="Show dots instead of lines")
        # Grid style
        self.OptionParser.add_option("--nv_grid_color_minor",
                                     action="store", type="int",
                                     dest="nv_grid_color_minor", default=1061158688,
                                     help="Minor grid line color")
        self.OptionParser.add_option("--nv_grid_color_major",
                                     action="store", type="int",
                                     dest="nv_grid_color_major", default=1061158720,
                                     help="Major grid line color")
        # Snapping
        self.OptionParser.add_option("--nv_snap_global",
                                     action="store", type="inkbool",
                                     dest="nv_snap_global", default=True,
                                     help="Global snapping")
        self.OptionParser.add_option("--nv_snap_perpendicular",
                                     action="store", type="inkbool",
                                     dest="nv_snap_perpendicular", default=False,
                                     help="Snap perpendicularly")
        self.OptionParser.add_option("--nv_snap_tangential",
                                     action="store", type="inkbool",
                                     dest="nv_snap_tangential", default=False,
                                     help="Snap tangentially")
        # Layer
        self.OptionParser.add_option("--nv_use_layers",
                                     action="store", type="string",
                                     dest="nv_use_layers", default="layers",
                                     help="Layer structure")
        self.OptionParser.add_option("--nv_custom_layer_name",
                                     action="store", type="string",
                                     dest="nv_custom_layer_name", default="Layer",
                                     help="Name for custom layer(s)")
        self.OptionParser.add_option("--nv_custom_layers",
                                     action="store", type="int",
                                     dest="nv_custom_layers", default=1,
                                     help="Number of custom layers")
        self.OptionParser.add_option("--nv_current_layer",
                                     action="store", type="string",
                                     dest="nv_current_layer", default="",
                                     help="Current layer (id)")
        # Misc
        # Test
        self.OptionParser.add_option("--nv_force_reload",
                                     action="store", type="inkbool",
                                     dest="nv_force_reload", default=True,
                                     help="Force document reload")
        self.OptionParser.add_option("--nv_testguides_u",
                                     action="store", type="string",
                                     dest="nv_testguides_u", default="cm",
                                     help="Test Guides Unit")
        self.OptionParser.add_option("--nv_testguides_x",
                                     action="store", type="float",
                                     dest="nv_testguides_x", default=2.5,
                                     help="Test Guides X")
        self.OptionParser.add_option("--nv_testguides_y",
                                     action="store", type="float",
                                     dest="nv_testguides_y", default=4.5,
                                     help="Test Guides Y")
        self.OptionParser.add_option("--nv_testguides_w",
                                     action="store", type="float",
                                     dest="nv_testguides_w", default=5.0,
                                     help="Test Guides Width")
        self.OptionParser.add_option("--nv_testguides_h",
                                     action="store", type="float",
                                     dest="nv_testguides_h", default=10.0,
                                     help="Test Guides Height")
        self.OptionParser.add_option("--nv_testguides_rect",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect", default=True,
                                     help="Test Guides around rect")
        self.OptionParser.add_option("--nv_testguides_rect_object",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect_object", default=True,
                                     help="Test Draw rectangle")
        self.OptionParser.add_option("--nv_testguides_hor",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_hor", default=True,
                                     help="Test horizontal guide")
        self.OptionParser.add_option("--nv_testguides_ver",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_ver", default=True,
                                     help="Test vertical guide")
        # Query
        self.OptionParser.add_option("--nv_doc_units",
                                     action="store", type="inkbool",
                                     dest="nv_doc_units", default=True,
                                     help="Query document display units")
        self.OptionParser.add_option("--nv_doc_page",
                                     action="store", type="inkbool",
                                     dest="nv_doc_page", default=True,
                                     help="Query document page size")
        self.OptionParser.add_option("--nv_doc_viewBox",
                                     action="store", type="inkbool",
                                     dest="nv_doc_viewBox", default=True,
                                     help="Query document viewBox values")
        self.OptionParser.add_option("--nv_doc_inkex",
                                     action="store", type="inkbool",
                                     dest="nv_doc_inkex", default=True,
                                     help="Query document size (inkex)")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")
        self.OptionParser.add_option("--notebook_page_size",
                                     action="store", type="string",
                                     dest="notebook_page_size")
        self.OptionParser.add_option("--notebook_user_units",
                                     action="store", type="string",
                                     dest="notebook_user_units")
        self.OptionParser.add_option("--notebook_grid_options",
                                     action="store", type="string",
                                     dest="notebook_grid_options")
        self.OptionParser.add_option("--notebook_grid_custom",
                                     action="store", type="string",
                                     dest="notebook_grid_custom")
        self.OptionParser.add_option("--notebook_grid_type",
                                     action="store", type="string",
                                     dest="notebook_grid_type")
        self.OptionParser.add_option("--notebook_grid_colors",
                                     action="store", type="string",
                                     dest="notebook_grid_colors")
        self.OptionParser.add_option("--notebook_guides_test",
                                     action="store", type="string",
                                     dest="notebook_guides_test")
        # scope
        self.OptionParser.add_option("--nv_use_dpi",
                                     action="store", type="string",
                                     dest="nv_use_dpi", default="96",
                                     help="Use internal resolution")
        self.OptionParser.add_option("--nv_use_guides_units",
                                     action="store", type="string",
                                     dest="nv_use_guides_units", default="96",
                                     help="Use internal resolution")
        self.OptionParser.add_option("--nv_set_all",
                                     action="store", type="inkbool",
                                     dest="nv_set_all", default=False,
                                     help="Apply in all tabs")
        self.OptionParser.add_option("--nv_verbose",
                                     action="store", type="inkbool",
                                     dest="nv_verbose", default=False,
                                     help="Verbose mode")
        self.OptionParser.add_option("--nv_query_only",
                                     action="store", type="inkbool",
                                     dest="nv_query_only", default=False,
                                     help="Query only (don't modify)")

    # methods for effect (per var / per tab / all)

    def query_attributes(self, node):
        # unpack globals
        page_width, page_width_unit, page_height, page_height_unit = self.page_size
        vb_width, vb_height = self.view_box[2:4]
        x_scale, y_scale = self.aspect_ratio[2:4]
        # inkscape version (runtime)
        inkex.debug("Inkscape version:\t%s\n"
                    % self.ink_vers)
        # globals: SVG user units
        inkex.debug("SVG user unit (x-scale, in page unit): \t%s (%s)"
                    % (formatted(x_scale), page_width_unit))
        inkex.debug("SVG user unit (y-scale, in page unit): \t%s (%s)"
                    % (formatted(y_scale), page_height_unit))
        inkex.debug("SVG user unit (x-scale, in CSS Pixel): \t%s (px)"
                    % formatted(x_scale * self.uuconv[page_width_unit]))
        inkex.debug("SVG user unit (y-scale, in CSS Pixel): \t%s (px)\n"
                    % formatted(y_scale * self.uuconv[page_height_unit]))
        # globals: page size
        page_unit = self.page_unit
        inkex.debug("Page unit:   \t%s"
                    % page_unit)
        inkex.debug("Page width:  \t%s"
                    % formatted(page_width))
        inkex.debug("Page height: \t%s\n"
                    % formatted(page_height))
        # globals: viewBox
        inkex.debug("SVG viewBox width:   \t%s"
                    % formatted(vb_width))
        inkex.debug("SVG viewBox height:  \t%s\n"
                    % formatted(vb_height))
        inkex.debug("preserveAspectRatio: \t%s\n"
                    % self.rt.get('preserveAspectRatio', "xMidYMid meet"))
        # globals: scale factor
        inkex.debug("Document scale x:  \t1 : %s"
                    % formatted(1.0 / x_scale))
        inkex.debug("Document scale y:  \t1 : %s\n"
                    % formatted(1.0 / y_scale))
        # document display units
        inkex.debug("Document Display unit: %s\n"
                    % self.nv.get(inkex.addNS('document-units', 'inkscape')))
        # inkex
        doc_unit = self.getDocumentUnit()
        doc_width, doc_height = (self.unittouu(self.rt.get(dim)) for dim in ['width', 'height'])
        inkex.debug("Document unit (inkex):   \t%s"
                    % doc_unit)
        inkex.debug("Document width (inkex):  \t%s"
                    % formatted(doc_width))
        inkex.debug("Document height (inkex): \t%s\n"
                    % formatted(doc_height))

    def set_attributes_size(self, node):

        def _get_page_options():
            page_w = self.options.page_width
            page_h = self.options.page_height
            page_u = self.options.nv_page_unit_id
            if self.options.notebook_page_size == '"tab_page_size_presets"':
                pagesize = doc_properties.get_format(self.options.nv_page_format,
                                                     self.options.nv_page_orientation)
                if pagesize:
                    page_w, page_h, page_u = pagesize
            return (page_w, page_h, page_u)

        def _get_viewbox_factor_from_preset(page_u):
            if self.options.svg_viewbox_scale == "auto":
                if self.rt.get('viewBox'):
                    vb_factor = 1.0
                else:
                    vb_factor = self.uuconv[page_u] / self.uuconv['px']
            else:
                vb_factor = self.uuconv[page_u] / self.uuconv[self.options.svg_viewbox_scale]
            return vb_factor

        type_ = None
        params = []

        page_w, page_h, page_u = _get_page_options()
        page_f = self.options.svg_page_scale_factor

        if page_f <= 0.0:
            inkex.errormsg("Page scale factor needs to be > 0. Falling back to 1.0")
            page_f = 1.0

        if self.options.notebook_user_units == '"tab_user_units_presets"':
            vb_f = _get_viewbox_factor_from_preset(page_u)
            type_ = 'page_scaled'
            params = [page_w, page_h, page_u, vb_f * page_f]
        elif self.options.notebook_user_units == '"tab_user_units_custom"':
            vb_f_x = self.options.svg_viewbox_scale_custom_x
            vb_f_y = (self.options.svg_viewbox_scale_custom_uniform
                      and vb_f_x
                      or self.options.svg_viewbox_scale_custom_y)
            type_ = 'page_scaled_non_isotrop'
            params = [page_w, page_u, vb_f_x * page_f, page_h, page_u, vb_f_y * page_f]
        elif self.options.notebook_user_units == '"tab_user_units_viewbox"':
            vb_str = self.options.svg_viewbox_custom_string
            vb_ratio = "%s %s" % (self.options.svg_viewbox_custom_aspect_align,
                                  self.options.svg_viewbox_custom_aspect_clip)
            type_ = 'page_scaled_viewbox'
            params = [page_w, page_h, page_u, page_f, vb_str, vb_ratio]
        else:
            type_ = 'page'
            params = [page_w, page_h, page_u]

        self.set_display_units(self.options.nv_display_unit_id)
        self.set_svg_size_with(type_, self.rt, params)

        if self.options.svg_viewbox_custom_rect == "rect":
            self.create_viewbox_rect()
        elif self.options.svg_viewbox_custom_rect == "clip":
            self.clip_layer_to_viewBox()
        else:
            pass

    def set_attributes_display(self, node):
        node.set('pagecolor',
                 self.options.nv_pagecolor)
        node.set(inkex.addNS('pageopacity', 'inkscape'),
                 str(round(self.options.nv_pageopacity, 2)))
        node.set(inkex.addNS('pagecheckerboard', 'inkscape'),
                 str(self.options.nv_pagecheckerboard))
        node.set('showborder',
                 str(self.options.nv_showborder))
        node.set('bordercolor',
                 self.options.nv_bordercolor)
        node.set('borderopacity',
                 str(round(self.options.nv_borderopacity, 2)))
        node.set('borderlayer',
                 str(self.options.nv_borderlayer))
        node.set(inkex.addNS('showpageshadow', 'inkscape'),
                 str(self.options.nv_showpageshadow))
        node.set(inkex.addNS('pageshadow', 'inkscape'),
                 self.options.nv_pageshadow)
        # rendering modes
        if self.options.nv_color_rendering != "unset":
            self.rt.set('color-rendering', self.options.nv_color_rendering)
        else:
            if self.rt.get('color-rendering'):
                del self.rt.attrib['color-rendering']
        if self.options.nv_shape_rendering != "unset":
            self.rt.set('shape-rendering', self.options.nv_shape_rendering)
        else:
            if self.rt.get('shape-rendering'):
                del self.rt.attrib['shape-rendering']
        if self.options.nv_text_rendering != "unset":
            self.rt.set('text-rendering', self.options.nv_text_rendering)
        else:
            if self.rt.get('text-rendering'):
                del self.rt.attrib['text-rendering']
        if self.options.nv_image_rendering != "unset":
            self.rt.set('image-rendering', self.options.nv_image_rendering)
        else:
            if self.rt.get('image-rendering'):
                del self.rt.attrib['image-rendering']

    def set_attributes_window(self, node):
        node.set(inkex.addNS('window-width', 'inkscape'),
                 str(self.options.nv_window_width))
        node.set(inkex.addNS('window-height', 'inkscape'),
                 str(self.options.nv_window_height))
        node.set(inkex.addNS('window-x', 'inkscape'),
                 str(self.options.nv_window_x))
        node.set(inkex.addNS('window-y', 'inkscape'),
                 str(self.options.nv_window_y))
        node.set(inkex.addNS('window-maximized', 'inkscape'),
                 str(self.options.nv_window_maximized))

    def set_attributes_zoom(self, node):
        self.set_zoom_to_page(self.options.nv_zoom / 100)

    def set_attributes_guides(self, node):
        node.set('showguides',
                 str(self.options.nv_showguides))
        node.set('guidecolor',
                 self.options.nv_guidecolor)
        node.set('guideopacity',
                 str(round(self.options.nv_guideopacity, 2)))
        node.set('guidehicolor',
                 self.options.nv_guidehicolor)
        node.set('guidehiopacity',
                 str(round(self.options.nv_guidehiopacity, 2)))
        if self.options.nv_guides_around_page:
            self.create_guides_around_page()

    def set_attributes_grids(self, node):

        def _set_custom_grid_options(grid, guid="grid"):
            gcolor, gopacity = get_color_from_int(self.options.nv_grid_color_minor)
            gempcolor, gempopacity = get_color_from_int(self.options.nv_grid_color_major)
            grid.set('id',
                     guid)
            grid.set('enabled',
                     inkbool2string(self.options.nv_grid_enabled))
            grid.set('visible',
                     inkbool2string(self.options.nv_grid_visible))
            grid.set('snapvisiblegridlinesonly',
                     inkbool2string(self.options.nv_grid_snapvisiblegridlinesonly))
            grid.set('empspacing',
                     str(self.options.nv_grid_empspacing))
            grid.set('dotted',
                     inkbool2string(self.options.nv_grid_dotted))
            grid.set('color',
                     gcolor)
            grid.set('opacity',
                     str(round(gopacity, 3)))
            grid.set('empcolor',
                     gempcolor)
            grid.set('empopacity',
                     str(round(gempopacity, 3)))

        def _set_preset_grid(grid=None):
            guid = ""
            type_ = self.options.nv_grid_preset_type
            if self.options.nv_grid_preset == "display":
                guid = self.uniqueId("DisplayGrid")
                grid = self.create_grid_with_display_units(type_)
            elif self.options.nv_grid_preset == "uu":
                guid = self.uniqueId("uuGrid")
                grid = self.create_grid_with_user_units(type_)
            elif self.options.nv_grid_preset == "page":
                guid = self.uniqueId("PageGrid")
                grid = self.create_page_grid(type_, self.page_unit, [0, 0, 1.0, 1.0, 30, 30])
            elif self.options.nv_grid_preset == "pixel":
                guid = self.uniqueId("PixelGrid")
                grid = self.create_page_grid("xygrid", "px", [0, 0, 0.5, 0.5, None, None])
                if grid is not None:
                    grid.set('empspacing', "2")
            else:
                guid = self.uniqueId("DefaultGrid")
                grid = self.create_default_grid(type_)
            if grid is not None:
                grid.set('id', guid)
            return grid

        def _set_custom_grid(grid=None):
            guid = ""
            if self.options.notebook_grid_type == '"tab_nv_grid_type_axo"':
                u = self.options.nv_grid_axo_units
                v = [self.options.nv_grid_axo_originx, self.options.nv_grid_axo_originy,
                     0, self.options.nv_grid_axo_spacingy,
                     self.options.nv_grid_axo_anglex, self.options.nv_grid_axo_angley]
                grid = self.create_page_grid("axonomgrid", u, v)
                guid = self.uniqueId("PageGridA")
            elif self.options.notebook_grid_type == '"tab_nv_grid_type_rect"':
                u = self.options.nv_grid_rect_units
                v = [self.options.nv_grid_rect_originx, self.options.nv_grid_rect_originy,
                     self.options.nv_grid_rect_spacingx, self.options.nv_grid_rect_spacingy,
                     0, 0]
                grid = self.create_page_grid("xygrid", u, v)
                guid = self.uniqueId("PageGridR")
            else:
                pass
            if grid is not None:
                _set_custom_grid_options(grid, guid)
            return grid

        if self.options.nv_creategrid:
            if self.options.notebook_grid_options == '"tab_nv_grid_presets"':
                grid = _set_preset_grid()
            elif self.options.notebook_grid_options == '"tab_nv_grid_custom"':
                grid = _set_custom_grid()
            else:
                pass

        # FIXME: setting 'showgrid' to true has no effect on current window
        node.set('showgrid', inkbool2string(self.options.nv_showgrid))

    def set_attributes_snapping(self, node):
        # deprecated (unused) nv attributes for snapping:
        # - guide-bbox, grid-bbox, object-bbox
        # - guide-points, grid-points, object-points
        # see also:
        #   <http://bazaar.launchpad.net/~inkscape.dev/inkscape/trunk/revision/2868>
        #   <http://bazaar.launchpad.net/~inkscape.dev/inkscape/trunk/revision/4250>
        #
        # TODO: more options
        node.set(inkex.addNS('snap-global', 'inkscape'),
                 str(self.options.nv_snap_global))
        node.set(inkex.addNS('snap-perpendicular', 'inkscape'),
                 str(self.options.nv_snap_perpendicular))
        node.set(inkex.addNS('snap-tangential', 'inkscape'),
                 str(self.options.nv_snap_tangential))

    def set_attributes_layer(self, node):
        if self.options.nv_use_layers == "svgroot":
            self.remove_layers(self.rt)
            current_layer = self.document.getroot().get('id')
        elif self.options.nv_use_layers == "custom":
            self.remove_layers(self.rt)
            current_layer = self.add_layers(self.options.nv_custom_layer_name,
                                            self.options.nv_custom_layers)
        else:
            current_layer = node.get(inkex.addNS('current-layer', 'inkscape'))
        if self.options.nv_current_layer:  # override current layer
            current_layer = self.options.nv_current_layer
        node.set(inkex.addNS('current-layer', 'inkscape'),
                 current_layer)

    def set_attributes_misc(self, node):
        pass

    def set_attributes_test(self, node):

        def _force_document_reload():
            self.rt.set('id', self.uniqueId("SVGRoot_"))

        def _create_layer(label):
            layer = inkex.etree.SubElement(self.rt, 'g')
            layer.set(inkex.addNS('label', 'inkscape'), label)
            layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
            return layer

        def _create_guides_from_preset():
            rect1 = (2.5, 4.5, 5.0, 10.0, 'cm')
            rect2 = (4.0, 1.5, 2.0, 3.0, 'in')
            rect3 = (650.0, 20.0, 100.0, 50.0, 'px')
            if self.options.nv_testguides_rect_object:
                parent = _create_layer('Guide test')
            for rect in [rect1, rect2, rect3]:
                if self.options.nv_testguides_rect:
                    self.create_guides_around_rect(*rect)
                if self.options.nv_testguides_rect_object:
                    self.create_test_rect(parent, *rect)
                if self.options.nv_testguides_hor:
                    y = self._unit2unit(rect[1], rect[4], self.page_unit)
                    self.create_horizontal_guideline("horizontal", y)
                if self.options.nv_testguides_ver:
                    x = self._unit2unit(rect[0], rect[4], self.page_unit)
                    self.create_vertical_guideline("vertical", x)

        def _create_guides_from_custom():
            x, y = (self.options.nv_testguides_x, self.options.nv_testguides_y)
            w, h = (self.options.nv_testguides_w, self.options.nv_testguides_h)
            u = self.options.nv_testguides_u
            if u == "display":
                u = node.get(inkex.addNS('document-units', 'inkscape'))
            if not u:
                u = "px"
            if self.options.nv_testguides_rect:
                self.create_guides_around_rect(x, y, w, h, u)
            if self.options.nv_testguides_rect_object:
                parent = _create_layer('Guide test')
                self.create_test_rect(parent, x, y, w, h, u)
            if self.options.nv_testguides_hor:
                y = self._unit2unit(y, u, self.page_unit)
                self.create_horizontal_guideline("horizontal", y)
            if self.options.nv_testguides_ver:
                x = self._unit2unit(x, u, self.page_unit)
                self.create_vertical_guideline("vertical", x)

        if self.options.notebook_guides_test == '"tab_nv_testguides_reload"':
            _force_document_reload()
        elif self.options.notebook_guides_test == '"tab_nv_testguides_presets"':
            _create_guides_from_preset()
        elif self.options.notebook_guides_test == '"tab_nv_testguides_custom"':
            _create_guides_from_custom()
        else:
            pass

    def set_attributes_all(self, node):
        self.set_attributes_size(node)
        self.set_attributes_display(node)
        self.set_attributes_window(node)
        self.set_attributes_zoom(node)
        self.set_attributes_guides(node)
        self.set_attributes_grids(node)
        self.set_attributes_snapping(node)
        self.set_attributes_misc(node)
        self.set_attributes_test(node)
        # self.query_attributes(node)

    def doit(self, node):
        node.set('id', self.options.nv_id)
        if self.options.nv_query_only:
            self.query_attributes(node)
        elif self.options.nv_set_all:
            self.set_attributes_all(node)
        elif self.options.notebook_main == '"tab_size"':
            self.set_attributes_size(node)
        elif self.options.notebook_main == '"tab_display"':
            self.set_attributes_display(node)
        elif self.options.notebook_main == '"tab_window"':
            self.set_attributes_window(node)
            self.set_attributes_zoom(node)
        elif self.options.notebook_main == '"tab_zoom"':
            self.set_attributes_zoom(node)
        elif self.options.notebook_main == '"tab_guides"':
            self.set_attributes_guides(node)
        elif self.options.notebook_main == '"tab_grids"':
            self.set_attributes_grids(node)
        elif self.options.notebook_main == '"tab_snapping"':
            self.set_attributes_snapping(node)
        elif self.options.notebook_main == '"tab_layer"':
            self.set_attributes_layer(node)
        elif self.options.notebook_main == '"tab_misc"':
            self.set_attributes_misc(node)
        elif self.options.notebook_main == '"tab_test"':
            self.set_attributes_test(node)
        elif self.options.notebook_main == '"tab_query"':
            self.query_attributes(node)
        else:
            inkex.debug("active tab: %s" % self.options.notebook_main)
            inkex.debug("Unknown tab.")

    def set_attribs(self):

        global DEBUG
        DEBUG = self.options.nv_verbose

        nv = self.getNamedView()

        if DEBUG:
            nv_attributes_old = dict(nv.attrib)

        # support 90 and 96 dpi
        if self.options.nv_use_dpi == "90":
            self.uuconv = doc_properties.UUCONV_90DPI  # inkex.uuconv (0.48, 0.91pre)
        elif self.options.nv_use_dpi == "96":
            self.uuconv = doc_properties.UUCONV_96DPI  # inkex.Effect._Effect__uuconv (0.91+)
        else:
            self.uuconv = inkex.Effect._Effect__uuconv

        # override version to store guides in User units:
        if self.options.nv_use_guides_units == "uu":
            self.ink_vers = "0.91+devel"

        self.doit(nv)

        if DEBUG:
            nv_attributes_new = dict(nv.attrib)
            # compare_attributes(nv_attributes_old, nv_attributes_new)
            print_attributes(nv_attributes_old)
            print_attributes(nv_attributes_new)


if __name__ == '__main__':
    e = DocProps()
    e.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
