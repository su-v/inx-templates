#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
lp1397564.py
Inkscape debug extension to test viewBox scaling

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
import inkex
import re

# globals

uuconv_90dpi = {'in': 90.0,
                'pt': 1.25,
                'px': 1,
                'mm': 3.5433070866,
                'cm': 35.433070866,
                'm' : 3543.3070866,
                'km': 3543307.0866,
                'pc': 15.0,
                'yd': 3240,
                'ft': 1080}

uuconv_96dpi = {'in': 96.0,
                'pt': 1.33333333333,
                'px': 1.0,
                'mm': 3.77952755913,
                'cm': 37.7952755913,
                'm' : 3779.52755913,
                'km': 3779527.55913,
                'pc': 16.0,
                'yd': 3456.0,
                'ft': 1152.0}


def formatted(f):
    return format(f, '.8f').rstrip('0').rstrip('.')


# class
class MyTemplate(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.rt = None
        self.nv = None
        self.ink_vers = None
        self.page_size = []
        self.page_unit = None
        self.view_box = []
        self.uu2pixel = None
        self.uu2page = None
        self.uuconv = None

    # helper

    def get_inkscape_version(self):
        inkscape_version_string = self.rt.get(inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        try:
            inkscape_version = version_match.search(inkscape_version_string).groups()
        except:
            inkscape_version = "Unknown"
        return inkscape_version[0]

    def get_document_unit(self):
        if self.nv.get('units'):
            return self.nv.get('units')
        elif self.nv.get(inkex.addNS('document-units', 'inkscape')):
            return self.nv.get(inkex.addNS('document-units', 'inkscape'))
        else:
            return 'px'

    def are_near_relative(self, a, b, eps):
        if (a-b <= a*eps) and (a-b >= -a*eps):
            return True
        else:
            return False

    def get_attributes_size(self):
        width = self.rt.get('width')
        height = self.rt.get('height')
        value_match = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_match = re.compile('(%s)$' % '|'.join(self.uuconv.keys()))
        wv = hv = "100"
        wu = hu = "px"
        # mwv, mhv = (value_match.match(val).group() for val in [width, height])
        mwv = value_match.match(width)
        if mwv:
            wv = mwv.group()
        mhv = value_match.match(height)
        if mhv:
            hv = mhv.group()
        # mwu, mhu = (unit_match.search(val).group() for val in [width, height])
        mwu = unit_match.search(width)
        if mwu:
            wu = mwu.group()
        mhu = unit_match.search(height)
        if mhu:
            hu = mhu.group()
        return (wv, wu, hv, hu)

    def get_size(self):
        wv, wu, hv, hu = self.get_attributes_size()
        if wu == hu:
            if wu:
                u = wu
            else:
                u = "px"
        else:
            u = "px"
            wv = str(self.unittouu(wv + wu))
            hv = str(self.unittouu(hv + hu))
        if u == "%" or u == "em":
            u = "mm"
            wv = "210"
            hv = "297"
        return (float(wv), float(hv), u)

    def update_page_size(self, w, h, u):
        self.page_size[0] = w
        self.page_size[1] = h
        self.page_size[2] = u
        self.page_unit = u
        self.uu2pixel = (self.uuconv[u] * w) / self.view_box[0]
        self.uu2page = w / self.view_box[0]

    def get_viewbox(self):
        vbf = 1.0
        vbu = "px"
        vbw = vbh = ""
        w, h, u = self.page_size
        vbstr = self.rt.get('viewBox')
        if vbstr:
            vbnums = []
            for t in vbstr.split():
                try:
                    vbnums.append(float(t))
                except ValueError:
                    pass
            if len(vbnums) == 4:
                vbw = vbnums[2]
                vbh = vbnums[3]
            vbf = self.uuconv[u] * w / vbw
            eps = 0.01
            for key in self.uuconv:
                if self.are_near_relative(self.uuconv[key], vbf, eps):
                    vbu = key
                    vbf = self.uuconv[vbu]
        if not vbw:
            vbw = self.unittouu("%s%s" % (w, u))
        if not vbh:
            vbh = self.unittouu("%s%s" % (h, u))
        return (vbw, vbh, vbf, vbu)

    def update_view_box(self, w, h):
        self.view_box[0] = w
        self.view_box[1] = h
        self.uu2pixel = (self.uuconv[self.page_unit] * self.page_size[0]) / w
        self.uu2page = self.page_size[0] / w

    def get_uu2pixel(self):
        return (self.uuconv[self.page_unit] * self.page_size[0]) / self.view_box[0]

    def get_uu2page(self):
        return self.page_size[0] / self.view_box[0]

    def unit2unit(self, x, u1, u2):
        return self.uutounit(self.unittouu("%s%s" % (x, u1)), u2)

    def get_px(self, x, xu):
        return self.unit2unit(x, xu, 'px')

    def get_uu(self, x, xu):
        return self.unit2unit(x, xu, self.page_unit) / self.uu2page

    def get_inverted_y(self, y, u):
        return self.unit2unit(self.page_size[1], self.page_unit, u) - y

    # methods

    def create_test_rect(self, node, x, y, w, h, u):
        y = self.get_inverted_y(y + h, u)
        x, y = (self.get_uu(x, u), self.get_uu(y, u))
        w, h = (self.get_uu(w, u), self.get_uu(h, u))
        rect_attribs = {'x': str(formatted(x)), 'y': str(formatted(y)),
                        'width': str(formatted(w)), 'height': str(formatted(h)),
                        'style': "opacity:0.5", }
        return inkex.etree.SubElement(node, inkex.addNS('rect', 'svg'), rect_attribs)

    def create_guides_around_page(self):
        w, h, u = self.page_size
        self.create_guides_around_rect(0, 0, w, h, u)

    def create_guides_around_rect(self, x, y, w, h, u):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            # guides in trunk use SVG user units (rev >= 13769)
            x0, y0 = (self.get_uu(x, u), self.get_uu(y, u))
            x1, y1 = (self.get_uu(x + w, u), self.get_uu(y + h, u))
        else:
            # guides in 0.91.x use CSS px
            x0, y0 = (self.get_px(x, u), self.get_px(y, u))
            x1, y1 = (self.get_px(x + w, u), self.get_px(y + h, u))
        self.create_guideline("bottom", "0,%s" % x1, x0, y0)
        self.create_guideline("right", "-%s,0" % y1, x1, y0)
        self.create_guideline("top", "0,-%s" % x1, x1, y1)
        self.create_guideline("left", "%s,0" % y1, x0, y1)

    def create_horizontal_guideline(self, name, position):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self.get_uu(position, self.page_unit)
        else:
            position = self.get_px(position, self.page_unit)
        self.create_guideline(name, "0,1", 0, position)

    def create_vertical_guideline(self, name, position):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self.get_uu(position, self.page_unit)
        else:
            position = self.get_px(position, self.page_unit)
        self.create_guideline(name, "1,0", position, 0)

    def create_guideline(self, label, orientation, x, y):
        guide = inkex.etree.SubElement(self.nv, inkex.addNS('guide', 'sodipodi'))
        guide.set("orientation", orientation)
        guide.set("position", "%s,%s" % (formatted(x), formatted(y)))
        label = ""  # TODO: add switch for label vs coord vs none
        if label:
            guide.set(inkex.addNS('label', 'inkscape'), label)
        else:
            guide.set(inkex.addNS('label', 'inkscape'), " x=%s  y=%s "
                      % (formatted(x), formatted(y)))

    # use class and apply effect

    def use_myTemplate(self):
        pass

    def effect(self):
        try:
            self.uuconv = inkex.Effect._Effect__uuconv
        except:
            self.uuconv = inkex.uuconv
        # support 0.48
        if not hasattr(self, 'unittouu'):
            self.unittouu = inkex.unittouu
        if not hasattr(self, 'uutounit'):
            self.uutounit = inkex.uutounit
        if not hasattr(self, 'getDocumentUnit'):
            self.getDocumentUnit = self.get_document_unit
        # shorthand
        self.rt = self.document.getroot()
        self.nv = self.document.xpath('//sodipodi:namedview', namespaces=inkex.NSS)[0]
        # runtime inkscape version (for now, taken from SVGRoot)
        self.ink_vers = self.get_inkscape_version()
        # set global lists
        for v in self.get_size():
            self.page_size.append(v)
        for v in self.get_viewbox()[:2]:
            self.view_box.append(v)
        # set globals values
        self.page_unit = self.page_size[2]
        self.uu2pixel = self.get_uu2pixel()
        self.uu2page = self.get_uu2page()
        # do it!
        self.use_myTemplate()


class C(MyTemplate):
    '''
    Test how to handle viewBox scale for object creation
    '''
    def __init__(self):
        MyTemplate.__init__(self)

        # Guides
        self.OptionParser.add_option("--nv_showguides",
                                     action="store", type="inkbool",
                                     dest="nv_showguides", default="True",
                                     help="Show Guides")
        self.OptionParser.add_option("--nv_guidecolor",
                                     action="store", type="string",
                                     dest="nv_guidecolor", default="#0000ff",
                                     help="Guide color")
        self.OptionParser.add_option("--nv_guideopacity",
                                     action="store", type="float",
                                     dest="nv_guideopacity", default="0.4",
                                     help="Guide opacity")
        self.OptionParser.add_option("--nv_guidehicolor",
                                     action="store", type="string",
                                     dest="nv_guidehicolor", default="#ff0000",
                                     help="Guide hilight color")
        self.OptionParser.add_option("--nv_guidehiopacity",
                                     action="store", type="float",
                                     dest="nv_guidehiopacity", default="0.4",
                                     help="Guide hilight opacity")
        self.OptionParser.add_option("--nv_guides_around_page",
                                     action="store", type="inkbool",
                                     dest="nv_guides_around_page", default="False",
                                     help="Guides around page")
        # Test
        self.OptionParser.add_option("--nv_testguides_u",
                                     action="store", type="string",
                                     dest="nv_testguides_u", default="cm",
                                     help="Test Guides Unit")
        self.OptionParser.add_option("--nv_testguides_x",
                                     action="store", type="float",
                                     dest="nv_testguides_x", default="2.5",
                                     help="Test Guides X")
        self.OptionParser.add_option("--nv_testguides_y",
                                     action="store", type="float",
                                     dest="nv_testguides_y", default="4.5",
                                     help="Test Guides Y")
        self.OptionParser.add_option("--nv_testguides_w",
                                     action="store", type="float",
                                     dest="nv_testguides_w", default="5.0",
                                     help="Test Guides Width")
        self.OptionParser.add_option("--nv_testguides_h",
                                     action="store", type="float",
                                     dest="nv_testguides_h", default="10.0",
                                     help="Test Guides Height")
        self.OptionParser.add_option("--nv_testguides_rect",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect", default="True",
                                     help="Test Guides around rect")
        self.OptionParser.add_option("--nv_testguides_rect_object",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect_object", default="True",
                                     help="Test Draw rectangle")
        self.OptionParser.add_option("--nv_testguides_hor",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_hor", default="True",
                                     help="Test horizontal guide")
        self.OptionParser.add_option("--nv_testguides_ver",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_ver", default="True",
                                     help="Test vertical guide")
        # Query
        self.OptionParser.add_option("--nv_doc_units",
                                     action="store", type="inkbool",
                                     dest="nv_doc_units", default="True",
                                     help="Query document display units")
        self.OptionParser.add_option("--nv_doc_page",
                                     action="store", type="inkbool",
                                     dest="nv_doc_page", default="True",
                                     help="Query document page size")
        self.OptionParser.add_option("--nv_doc_viewBox",
                                     action="store", type="inkbool",
                                     dest="nv_doc_viewBox", default="True",
                                     help="Query document viewBox values")
        self.OptionParser.add_option("--nv_doc_inkex",
                                     action="store", type="inkbool",
                                     dest="nv_doc_inkex", default="True",
                                     help="Query document size (inkex)")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")
        self.OptionParser.add_option("--notebook_guides_test",
                                     action="store", type="string",
                                     dest="notebook_guides_test")
        # scope
        self.OptionParser.add_option("--nv_use_dpi",
                                     action="store", type="string",
                                     dest="nv_use_dpi", default="96",
                                     help="Use internal resolution")
        self.OptionParser.add_option("--nv_use_guides_units",
                                     action="store", type="string",
                                     dest="nv_use_guides_units", default="96",
                                     help="Use internal resolution")

    def run_test(self, node):
        node.set('showguides', str(self.options.nv_showguides))
        if self.options.nv_guides_around_page:
            self.create_guides_around_page()
        parent = self.rt
        if self.options.notebook_guides_test == '"tab_nv_testguides_presets"':
            rect1 = (2.5, 4.5, 5.0, 10.0, 'cm')
            rect2 = (4.0, 1.5, 2.0, 3.0, 'in')
            rect3 = (650.0, 20.0, 100.0, 50.0, 'px')
            for rect in [rect1, rect2, rect3]:
                if self.options.nv_testguides_rect:
                    self.create_guides_around_rect(*rect)
                if self.options.nv_testguides_rect_object:
                    self.create_test_rect(parent, *rect)
                if self.options.nv_testguides_hor:
                    y = self.unit2unit(rect[1], rect[4], self.page_unit)
                    self.create_horizontal_guideline("horizontal", y)
                if self.options.nv_testguides_ver:
                    x = self.unit2unit(rect[0], rect[4], self.page_unit)
                    self.create_vertical_guideline("vertical", x)
        else:
            x, y = (self.options.nv_testguides_x, self.options.nv_testguides_y)
            w, h = (self.options.nv_testguides_w, self.options.nv_testguides_h)
            u = self.options.nv_testguides_u
            if u == "display":
                u = node.get(inkex.addNS('document-units', 'inkscape'))
            if not u:
                u = "px"
            if self.options.nv_testguides_rect:
                self.create_guides_around_rect(x, y, w, h, u)
            if self.options.nv_testguides_rect_object:
                self.create_test_rect(parent, x, y, w, h, u)
            if self.options.nv_testguides_hor:
                y = self.unit2unit(y, u, self.page_unit)
                self.create_horizontal_guideline("horizontal", y)
            if self.options.nv_testguides_ver:
                x = self.unit2unit(x, u, self.page_unit)
                self.create_vertical_guideline("vertical", x)

    def query_attributes(self, node):
        inkex.debug("Inkscape version:\t%s\n"
                    % self.ink_vers)
        display_unit = self.nv.get(inkex.addNS('document-units', 'inkscape'))
        inkex.debug("Document Display unit: %s\n" % display_unit)

        inkex.debug("Document unit (from inkex):    \t%s"
                    % self.getDocumentUnit())
        inkex.debug("SVG user unit = x * CSS Pixel: \t%s"
                    % formatted(self.uu2pixel))
        inkex.debug("SVG user unit = y * page unit: \t%s\n"
                    % formatted(self.uu2page))
        page_width, page_height, page_unit = self.page_size
        inkex.debug("Page unit:   \t%s"
                    % page_unit)
        inkex.debug("Page width:  \t%s"
                    % formatted(page_width))
        inkex.debug("Page height: \t%s\n"
                    % formatted(page_height))
        vb_width, vb_height, unit_factor, unit_match = self.get_viewbox()
        inkex.debug("SVG user unit (in CSS Pixels): \t%s%s"
                    % (formatted(unit_factor), 'px'))
        inkex.debug("SVG user unit (in Page unit):  \t%s%s"
                    % (formatted(self.unit2unit(unit_factor, 'px', page_unit)), page_unit))
        inkex.debug("SVG viewBox width:   \t%s"
                    % formatted(vb_width))
        inkex.debug("SVG viewBox height:  \t%s\n"
                    % formatted(vb_height))
        doc_width, doc_height = (self.unittouu(self.rt.get(dim)) for dim in ['width', 'height'])
        doc_unit = self.getDocumentUnit()
        inkex.debug("Document unit (inkex):   \t%s"
                    % doc_unit)
        inkex.debug("Document width (inkex):  \t%s"
                    % formatted(doc_width))
        inkex.debug("Document height (inkex): \t%s\n"
                    % formatted(doc_height))
        inkex.debug("Document scale:  \t1 : %s\n"
                    % formatted(vb_width / page_width))

    def doit(self, node):
        if self.options.notebook_main == '"tab_test"':
            self.run_test(node)
        elif self.options.notebook_main == '"tab_query"':
            self.query_attributes(node)
        else:
            inkex.debug("active tab: %s" % self.options.tab)
            inkex.debug("This tab does not apply an effect.")

    def use_myTemplate(self):
        # support 90 and 96 dpi
        if self.options.nv_use_dpi == "90":
            self.uuconv = uuconv_90dpi  # inkex.uuconv (0.48, 0.91pre)
        elif self.options.nv_use_dpi == "96":
            self.uuconv = uuconv_96dpi  # inkex.Effect._Effect__uuconv (0.91+)
        # override version to store guides in User units:
        if self.options.nv_use_guides_units == "uu":
            self.ink_vers = "0.91+devel"
        # do things
        self.doit(self.nv)


if __name__ == '__main__':
    effect = C()
    effect.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
