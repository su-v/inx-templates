#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
lp1397564-2.py
Inkscape debug extension to test viewBox scaling

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=bad-whitespace
# pylint: disable=fixme
# pylint: disable=broad-except
# pylint: disable=no-member

# import
import inkex
import re

# globals

DEBUG = 1

UUCONV_90DPI = {'in':90.0,
                'pt':1.25,
                'px':1,
                'mm':3.5433070866,
                'cm':35.433070866,
                'm':3543.3070866,
                'km':3543307.0866,
                'pc':15.0,
                'yd':3240,
                'ft':1080}

UUCONV_96DPI = {'in':96.0,
                'pt':1.33333333333,
                'px':1.0,
                'mm':3.77952755913,
                'cm':37.7952755913,
                'm':3779.52755913,
                'km':3779527.55913,
                'pc':16.0,
                'yd':3456.0,
                'ft':1152.0}


INKSCAPE_PAPERSIZES = {'default'                 : (   210,   297, "mm"),
                       'A4'                      : (   210,   297, "mm"),
                       'US Letter'               : (   8.5,    11, "in"),
                       'US Legal'                : (   8.5,    14, "in"),
                       'US Executive'            : (  7.25,  10.5, "in"),
                       'A0'                      : (   841,  1189, "mm"),
                       'A1'                      : (   594,   841, "mm"),
                       'A2'                      : (   420,   594, "mm"),
                       'A3'                      : (   297,   420, "mm"),
                       'A5'                      : (   148,   210, "mm"),
                       'A6'                      : (   105,   148, "mm"),
                       'A7'                      : (    74,   105, "mm"),
                       'A8'                      : (    52,    74, "mm"),
                       'A9'                      : (    37,    52, "mm"),
                       'A10'                     : (    26,    37, "mm"),
                       'B0'                      : (  1000,  1414, "mm"),
                       'B1'                      : (   707,  1000, "mm"),
                       'B2'                      : (   500,   707, "mm"),
                       'B3'                      : (   353,   500, "mm"),
                       'B4'                      : (   250,   353, "mm"),
                       'B5'                      : (   176,   250, "mm"),
                       'B6'                      : (   125,   176, "mm"),
                       'B7'                      : (    88,   125, "mm"),
                       'B8'                      : (    62,    88, "mm"),
                       'B9'                      : (    44,    62, "mm"),
                       'B10'                     : (    31,    44, "mm"),
                       'C0'                      : (   917,  1297, "mm"),
                       'C1'                      : (   648,   917, "mm"),
                       'C2'                      : (   458,   648, "mm"),
                       'C3'                      : (   324,   458, "mm"),
                       'C4'                      : (   229,   324, "mm"),
                       'C5'                      : (   162,   229, "mm"),
                       'C6'                      : (   114,   162, "mm"),
                       'C7'                      : (    81,   114, "mm"),
                       'C8'                      : (    57,    81, "mm"),
                       'C9'                      : (    40,    57, "mm"),
                       'C10'                     : (    28,    40, "mm"),
                       'D1'                      : (   545,   771, "mm"),
                       'D2'                      : (   385,   545, "mm"),
                       'D3'                      : (   272,   385, "mm"),
                       'D4'                      : (   192,   272, "mm"),
                       'D5'                      : (   136,   192, "mm"),
                       'D6'                      : (    96,   136, "mm"),
                       'D7'                      : (    68,    96, "mm"),
                       'E3'                      : (   400,   560, "mm"),
                       'E4'                      : (   280,   400, "mm"),
                       'E5'                      : (   200,   280, "mm"),
                       'E6'                      : (   140,   200, "mm"),
                       'CSE'                     : (   462,   649, "pt"),
                       'US #10 Envelope'         : ( 4.125,   9.5, "in"),
                       'DL Envelope'             : (   110,   220, "mm"),
                       'Ledger/Tabloid'          : (    11,    17, "in"),
                       'Banner 468x60'           : (    60,   468, "px"),
                       'Icon 16x16'              : (    16,    16, "px"),
                       'Icon 32x32'              : (    32,    32, "px"),
                       'Icon 48x48'              : (    48,    48, "px"),
                       'Business Card (ISO 7810)': ( 53.98, 85.60, "mm"),
                       'Business Card (US)'      : (     2,   3.5, "in"),
                       'Business Card (Europe)'  : (    55,    85, "mm"),
                       'Business Card (Aus/NZ)'  : (    55,    90, "mm"),
                       'Arch A'                  : (     9,    12, "in"),
                       'Arch B'                  : (    12,    18, "in"),
                       'Arch C'                  : (    18,    24, "in"),
                       'Arch D'                  : (    24,    36, "in"),
                       'Arch E'                  : (    36,    48, "in"),
                       'Arch E1'                 : (    30,    42, "in")}


def get_page_size_from_format(page_format, page_orientation):
    try:
        known_papersize = INKSCAPE_PAPERSIZES[page_format]
    except KeyError, error_msg:
        inkex.debug("Unknpwn paper format, using fallback A4. (Error: %s)." % error_msg)
        known_papersize = INKSCAPE_PAPERSIZES['default']
    unit = known_papersize[2]
    if page_orientation == "portrait":
        width = float(known_papersize[0])
        height = float(known_papersize[1])
    else:
        width = float(known_papersize[1])
        height = float(known_papersize[0])
    return (width, height, unit)


def formatted(f):
    return format(f, '.8f').rstrip('0').rstrip('.')


def inkbool2string(inkbool):
    if inkbool:
        return "true"
    else:
        return "false"


# class
class MyTemplate(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.uuconv = None
        self.rt = None
        self.nv = None
        self.ink_vers = None
        self.page_size = []
        self.view_box = []
        self.aspect_ratio = []
        # legacy
        self.page_unit = None

    # helper: misc

    def _get_inkscape_version(self):
        """returns runtime version of Inkscape based on 'inkscape:version' attribute"""
        inkscape_version_string = self.rt.get(inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    def get_document_unit(self):
        """legacy method for backwards compatibility with 0.48.x"""
        if 'units' in self.nv.attrib:
            return self.nv.get('units')
        elif inkex.addNS('document-units', 'inkscape') in self.nv.attrib:
            return self.nv.get(inkex.addNS('document-units', 'inkscape'))
        else:
            return 'px'

    # helper: unit conversion between real-world units (absolute)

    def _unit2csspx(self, q, u):
        return q * self.uuconv[u]

    def _csspx2unit(self, q, u):
        return q / self.uuconv[u]

    def _unit2unit(self, q, u1, u2):
        return (q * self.uuconv[u1]) / self.uuconv[u2]

    def _get_px(self, q, u):
        return self._unit2unit(q, u, 'px')

    def _get_inverted_y(self, y, u):
        """gets height in page unit, returns y offset in specified unit"""
        return self._unit2unit(self.page_size[2], self.page_size[3], u) - y

    def _get_offset_x(self, q, u):
        """gets viewBox origin x from _get_aspect() in page units,
           returns offset in specified unit"""
        return q - self._unit2unit(self.aspect_ratio[0], self.page_size[1], u)

    def _get_offset_y(self, q, u):
        """gets viewBox origin y from _get_aspect() in page units,
           returns offset in specified unit"""
        return q - self._unit2unit(self.aspect_ratio[1], self.page_size[3], u)

    # helper: unit relations for SVG user units to page unit

    # aspect_align == none

    def _get_uu_x_raw(self, q, u):
        """returns quantity in user units based on page width / viewBox width ratio"""
        # use for guides in current trunk and patched 0.91pre3
        return self._unit2unit(q, u, self.page_size[1]) / (self.page_size[0] / self.view_box[2])

    def _get_uu_y_raw(self, q, u):
        """returns quantity in user units based on page height / viewBox height ratio"""
        # use for guides in current trunk and patched 0.91pre3"""
        return self._unit2unit(q, u, self.page_size[3]) / (self.page_size[2] / self.view_box[3])

    def _get_uu_rect_raw(self, params, u):
        x, y, w, h = params
        # offset (inverted desktop coords)
        y = self._get_inverted_y(y + h, u)
        # convert to user units
        x, y = (self._get_uu_x_raw(x, u), self._get_uu_y_raw(y, u))
        w, h = (self._get_uu_x_raw(w, u), self._get_uu_y_raw(h, u))
        # add viewBox x, y (in user units)
        x += self.view_box[0]
        y += self.view_box[1]
        return (x, y, w, h)

    # aspect_align != none

    def _get_uu_x(self, q, u):
        """returns quantity in user units, adjusted by x-scale based on preserveAspectRatio"""
        return self._unit2unit(q, u, self.page_size[1]) / self.aspect_ratio[2]

    def _get_uu_y(self, q, u):
        """returns quantity in user units, adjusted by y-scale based on preserveAspectRatio"""
        return self._unit2unit(q, u, self.page_size[3]) / self.aspect_ratio[3]

    def _get_uu_rect(self, params, u):
        x, y, w, h = params
        # offset (inverted desktop coords)
        y = self._get_inverted_y(y + h, u)
        # offset (viewBox, preserveAspectRatio, based on page unit, returned in specified units)
        x = self._get_offset_x(x, u)
        y = self._get_offset_y(y, u)
        # convert to user units
        x, y = (self._get_uu_x(x, u), self._get_uu_y(y, u))
        w, h = (self._get_uu_x(w, u), self._get_uu_y(h, u))
        # add viewBox x, y (in user units)
        x += self.view_box[0]
        y += self.view_box[1]
        return (x, y, w, h)

    # helper: store current document size, unit and viewBox

    def _update_globals(self):
        self.aspect_ratio = list(self._get_aspect())
        # legacy
        self.page_unit = self.page_size[1]

    def _update_page_size(self, params, update_static=False):
        w, wu, h, hu = params
        self.page_size[0:2] = [float(w), wu]
        self.page_size[2:4] = [float(h), hu]
        if update_static:
            self._update_globals()

    def _update_view_box(self, params, update_static=False):
        x, y, w, h = params
        self.view_box[0:2] = [float(x), float(y)]
        self.view_box[2:4] = [float(w), float(h)]
        if update_static:
            self._update_globals()

    def _update_page_size_and_view_box(self, page, viewbox, update_static=True):
        self._update_page_size(page)
        self._update_view_box(viewbox)
        if update_static:
            self._update_globals()

    # helper: get scale factors for current document

    def _get_aspect(self):
        """returns list of 2 lists: 'offset' [x, y] and 'scale' [scale_x, scale_y] """
        w, wu, h, hu = self.page_size
        vbx, vby, vbw, vbh = self.view_box
        x = y = 0.0
        width, height = [w, h]
        scale = 1.0
        aspect_align, aspect_clip = self.rt.get('preserveAspectRatio', "xMidYMid meet").split()
        if aspect_align != "none":
            scale_x = w / vbw
            scale_y = h / vbh
            if aspect_clip == "meet":
                scale = min(scale_x, scale_y)
            else:  # slice
                scale = max(scale_x, scale_y)
            width = vbw * scale
            height = vbh * scale
            if aspect_align == "xMinYMin":
                pass
            elif aspect_align == "xMidYMin":
                x = 0.5 * (w - width)
            elif aspect_align == "xMaxYMin":
                x = 1.0 * (w - width)
            elif aspect_align == "xMinYMid":
                y = 0.5 * (h - height)
            elif aspect_align == "xMidYMid":
                x = 0.5 * (w - width)
                y = 0.5 * (h - height)
            elif aspect_align == "xMaxYMid":
                x = 1.0 * (w - width)
                y = 0.5 * (h - height)
            elif aspect_align == "xMinYMax":
                y = 1.0 * (h - height)
            elif aspect_align == "xMidYMax":
                x = 0.5 * (w - width)
                y = 1.0 * (h - height)
            elif aspect_align == "xMaxYMax":
                x = 1.0 * (w - width)
                y = 1.0 * (h - height)
        return (x, y, width / vbw, height / vbh)

    # helper: page size, units, viewBox, window

    def _get_attributes_size(self):
        width = self.rt.get('width')
        height = self.rt.get('height')
        value_match = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_match = re.compile('(%s)$' % '|'.join(self.uuconv.keys()))
        wv = hv = "100"
        wu = hu = "px"
        # mwv, mhv = (value_match.match(val).group() for val in [width, height])
        mwv = value_match.match(width)
        if mwv:
            wv = mwv.group()
        mhv = value_match.match(height)
        if mhv:
            hv = mhv.group()
        # mwu, mhu = (unit_match.search(val).group() for val in [width, height])
        mwu = unit_match.search(width)
        if mwu:
            wu = mwu.group()
        mhu = unit_match.search(height)
        if mhu:
            hu = mhu.group()
        return (wv, wu, hv, hu)

    def _get_pagesize(self):
        wv, wu, hv, hu = self._get_attributes_size()
        # fallback (missing unit identifier)
        if not wu:
            wu = "px"
        if not hu:
            hu = "px"
        # FIXME: relative units: %
        if wu == "%":
            wu = "mm"
            wv = 210.0 * (float(wv) / 100.0)
        if hu == "%":
            hu = "mm"
            hv = 297.0 * (float(hv) / 100.0)
        # FIXME: not yet supported: em
        if wu == "em":
            wu = "px"
            wv = "100"
        if hu == "em":
            hu = "px"
            hv = "100"
        return (float(wv), wu, float(hv), hu)

    def _get_viewbox(self):
        w, wu, h, hu = self.page_size
        vbx = vby = 0.0
        vbw = float(self._unit2csspx(w, wu))
        vbh = float(self._unit2csspx(h, hu))
        vbstr = self.rt.get('viewBox', None)
        if vbstr is not None:
            try:
                vbx, vby, vbw, vbh = (float(i) for i in vbstr.split())
            except ValueError, error_msg:
                if DEBUG:
                    inkex.errormsg("Failed to parse viewBox attribute ('%s'): %s"
                                   % (vbstr, error_msg))
                    inkex.errormsg("Fallback values for viewBox are:")
                    inkex.errormsg("vbx: %f, vby: %f, vbw: %f, vbh: %f"
                                   % (vbx, vby, vbw, vbh))
                else:
                    pass
        return (vbx, vby, vbw, vbh)

    # methods: page size, units, window

    def _set_svg_size(self, node, page, viewbox, ratio=None):
        node.set('id', "SVGRoot")
        node.set('width', "%s%s" % (formatted(page[0]), page[1]))
        node.set('height', "%s%s" % (formatted(page[2]), page[3]))
        node.set('viewBox', "%s %s %s %s" % tuple(formatted(v) for v in viewbox))
        if ratio is not None:
            node.set('preserveAspectRatio', ratio)
        elif 'preserveAspectRatio' in node.attrib:
            del node.attrib['preserveAspectRatio']
        self._update_page_size_and_view_box(page, viewbox)
        # inkex.debug(self._get_aspect())

    def set_svg_size_with(self, type_, node, params):
        if type_ == "whu":
            w, h, u = params
            self._set_svg_size(node, [w, u, h, u], [0.0, 0.0, w, h])
        elif type_ == "whuf":
            w, h, u, f = params
            self._set_svg_size(node, [w, u, h, u], [0.0, 0.0, w * f, h * f])
        elif type_ == "wufhuf":
            w, wu, fx, h, hu, fy = params
            self._set_svg_size(node, [w, wu, h, hu], [0.0, 0.0, w * fx, h * fy])
        elif type_ == "whufvbr":
            w, h, u, f, vb, r = params
            try:
                vbx, vby, vbw, vbh = [float(i) * f for i in vb.split()]
            except ValueError, error_msg:
                inkex.errormsg("Invalid viewBox '%s' string detected (Error: %s)."
                               % (vb, error_msg))
                inkex.errormsg("Falling back to viewBox based on page width and height.")
                vbx, vby, vbw, vbh = [0.0, 0.0, w * f, h * f]
            self._set_svg_size(node, [w, u, h, u], [vbx, vby, vbw, vbh], r)
        else:
            # pass
            inkex.debug("Unknown type_: %s" % type_)
            inkex.debug(params)

    # methods: namedView attributes

    def set_display_units(self, u):
        self.nv.set(inkex.addNS('document-units', 'inkscape'), u)

    def set_units(self, u):
        self.nv.set('units', u)

    # methods: layers

    def create_layer(self, label):
        layer = inkex.etree.SubElement(self.rt, 'g')
        layer.set('id', self.uniqueId("layer"))
        layer.set(inkex.addNS('label', 'inkscape'), label)
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
        return layer

    def set_current_layer(self, layer):
        """sets namedview attribute for current layer (layer needs to have 'id' attribute"""
        if 'id' in layer.attrib:
            self.getNamedView().set(inkex.addNS('current-layer', 'inkscape'), layer.get('id'))

    # private methods: guide lines

    def _create_guideline(self, label, orientation, x, y):
        guide = inkex.etree.SubElement(self.nv, inkex.addNS('guide', 'sodipodi'))
        guide.set("orientation", orientation)
        guide.set("position", "%s,%s" % (formatted(x), formatted(y)))
        label = ""  # TODO: add switch for label vs coord vs none
        if label:
            guide.set(inkex.addNS('label', 'inkscape'), label)
        else:
            guide.set(inkex.addNS('label', 'inkscape'), " x=%s  y=%s "
                      % (formatted(x), formatted(y)))

    def _create_guides_around_rect_xy(self, x, y, w, h, wu, hu):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            # guides in trunk use SVG user units (rev >= 13769)
            x0, y0 = (self._get_uu_x_raw(x, wu), self._get_uu_y_raw(y, hu))
            x1, y1 = (self._get_uu_x_raw(x + w, wu), self._get_uu_y_raw(y + h, hu))
        else:
            # guides in 0.91.x use CSS px
            x0, y0 = (self._get_px(x, wu), self._get_px(y, hu))
            x1, y1 = (self._get_px(x + w, wu), self._get_px(y + h, hu))
        self._create_guideline("bottom", "0,%s" % x1, x0, y0)
        self._create_guideline("right", "-%s,0" % y1, x1, y0)
        self._create_guideline("top", "0,-%s" % x1, x1, y1)
        self._create_guideline("left", "%s,0" % y1, x0, y1)

    # methods: guide lines

    def create_horizontal_guideline(self, name, position):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self._get_uu_y_raw(position, self.page_size[3])
        else:
            position = self._get_px(position, self.page_size[3])
        self._create_guideline(name, "0,1", 0, position)

    def create_vertical_guideline(self, name, position):
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self._get_uu_x_raw(position, self.page_size[1])
        else:
            position = self._get_px(position, self.page_size[1])
        self._create_guideline(name, "1,0", position, 0)

    def create_guides_around_rect(self, x, y, w, h, u):
        self._create_guides_around_rect_xy(x, y, w, h, u, u)

    def create_guides_around_page(self):
        w, wu, h, hu = self.page_size
        self._create_guides_around_rect_xy(0, 0, w, h, wu, hu)

    # methods: create and modify objects

    def create_test_rect(self, node, x, y, w, h, u):
        x, y, w, h = self._get_uu_rect([x, y, w, h], u)
        rect_attribs = {'x': str(formatted(x)), 'y': str(formatted(y)),
                        'width': str(formatted(w)), 'height': str(formatted(h)),
                        'style': "opacity:0.25", }
        return inkex.etree.SubElement(node, inkex.addNS('rect', 'svg'), rect_attribs)

    def create_viewbox_rect(self, node=None):
        if node is None:
            node = self.rt
        vbx, vby, vbw, vbh = self.view_box
        rect_attribs = {'x': str(formatted(vbx)), 'y': str(formatted(vby)),
                        'width': str(formatted(vbw)), 'height': str(formatted(vbh)),
                        'style': "fill:blue;opacity:0.10", 'id': self.uniqueId("viewbox"), }
        return inkex.etree.SubElement(node, inkex.addNS('rect', 'svg'), rect_attribs)

    def clip_layer_to_viewBox(self, layer=None):
        if layer is None:
            layer = self.create_layer("viewBox")
        vb_rect = self.create_viewbox_rect()
        clip_id = self.uniqueId("viewBox_clip")
        clip_attribs = {'clipPathUnits': "userSpaceOnUse", 'id': clip_id, }
        defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs is None:
            defs = inkex.etree.SubElement(self.document.getroot(), inkex.addNS('defs', 'svg'))
        vb_clip = inkex.etree.SubElement(defs, inkex.addNS('clipPath', 'svg'), clip_attribs)
        vb_clip.append(vb_rect)
        page_rect = self.create_test_rect(layer, 0, 0, self.page_size[0], self.page_size[2],
                                          self.page_size[1])
        page_rect.set('style', "fill:red;opacity:0.1")
        page_rect.set(inkex.addNS('insensitive', 'sodipodi'), "true")
        layer.set('clip-path', "url(#%s)" % clip_id)
        self.set_current_layer(layer)

    # use class and apply effect

    def use_myTemplate(self):
        pass

    def effect(self):
        try:
            self.uuconv = inkex.Effect._Effect__uuconv
        except AttributeError:
            self.uuconv = inkex.uuconv
        # support 0.48
        if not hasattr(self, 'unittouu'):
            self.unittouu = inkex.unittouu
        if not hasattr(self, 'uutounit'):
            self.uutounit = inkex.uutounit
        if not hasattr(self, 'getDocumentUnit'):
            self.getDocumentUnit = self.get_document_unit
        # shorthand
        self.rt = self.document.getroot()
        self.nv = self.document.xpath('//sodipodi:namedview', namespaces=inkex.NSS)[0]
        # runtime inkscape version (for now, taken from SVGRoot)
        self.ink_vers = self._get_inkscape_version()
        # set global lists
        self.page_size = list(self._get_pagesize())
        self.view_box = list(self._get_viewbox())
        # set globals with scale factors
        self.aspect_ratio = list(self._get_aspect())
        # legacy
        self.page_unit = self.page_size[1]  # width Page unit
        # do it!
        self.use_myTemplate()


class C(MyTemplate):
    '''
    Test how to handle viewBox scale for object creation
    '''
    def __init__(self):
        MyTemplate.__init__(self)

        # Id
        self.OptionParser.add_option("--nv_id",
                                     action="store", type="string",
                                     dest="nv_id", default="base",
                                     help="Id of namedview node")
        # Display unit
        self.OptionParser.add_option("--nv_display_unit_id",
                                     action="store", type="string",
                                     dest="nv_display_unit_id", default="mm",
                                     help="Document default unit identifier")
        # Page
        self.OptionParser.add_option("--nv_page_format",
                                     action="store", type="string",
                                     dest="nv_page_format", default="custom",
                                     help="Page format (presets)")
        self.OptionParser.add_option("--nv_page_orientation",
                                     action="store", type="string",
                                     dest="nv_page_orientation", default="portrait",
                                     help="Page orientation")
        self.OptionParser.add_option("--nv_page_unit_id",
                                     action="store", type="string",
                                     dest="nv_page_unit_id", default="mm",
                                     help="Page unit identifier")
        self.OptionParser.add_option("--page_width",
                                     action="store", type="float",
                                     dest="page_width", default=210.0,
                                     help="Page width")
        self.OptionParser.add_option("--page_height",
                                     action="store", type="float",
                                     dest="page_height", default=297.0,
                                     help="Page height")
        self.OptionParser.add_option("--svg_page_scale_factor",
                                     action="store", type="float",
                                     dest="svg_page_scale_factor", default=1.0,
                                     help="Page viewBox scale factor")
        # viewBox
        self.OptionParser.add_option("--svg_viewbox_scale",
                                     action="store", type="string",
                                     dest="svg_viewbox_scale", default="auto",
                                     help="Page viewBox scale mode")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_x",
                                     action="store", type="float",
                                     dest="svg_viewbox_scale_custom_x", default=1.0,
                                     help="Page viewBox custom scale x")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_y",
                                     action="store", type="float",
                                     dest="svg_viewbox_scale_custom_y", default=1.0,
                                     help="Page viewBox custom scale y")
        self.OptionParser.add_option("--svg_viewbox_scale_custom_uniform",
                                     action="store", type="inkbool",
                                     dest="svg_viewbox_scale_custom_uniform", default=True,
                                     help="Page viewBox custom uniform scaling")
        self.OptionParser.add_option("--svg_viewbox_custom_string",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_string", default="0 0 100 100",
                                     help="Page viewBox custom string")
        self.OptionParser.add_option("--svg_viewbox_custom_aspect_align",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_aspect_align", default="none",
                                     help="Page viewBox custom AspectRatio")
        self.OptionParser.add_option("--svg_viewbox_custom_aspect_clip",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_aspect_clip", default="meet",
                                     help="Page viewBox custom meetOrSlice")
        self.OptionParser.add_option("--svg_viewbox_custom_rect",
                                     action="store", type="string",
                                     dest="svg_viewbox_custom_rect", default="none",
                                     help="Page viewBox custom rect")
        # Guides
        self.OptionParser.add_option("--nv_showguides",
                                     action="store", type="inkbool",
                                     dest="nv_showguides", default="True",
                                     help="Show Guides")
        self.OptionParser.add_option("--nv_guidecolor",
                                     action="store", type="string",
                                     dest="nv_guidecolor", default="#0000ff",
                                     help="Guide color")
        self.OptionParser.add_option("--nv_guideopacity",
                                     action="store", type="float",
                                     dest="nv_guideopacity", default="0.4",
                                     help="Guide opacity")
        self.OptionParser.add_option("--nv_guidehicolor",
                                     action="store", type="string",
                                     dest="nv_guidehicolor", default="#ff0000",
                                     help="Guide hilight color")
        self.OptionParser.add_option("--nv_guidehiopacity",
                                     action="store", type="float",
                                     dest="nv_guidehiopacity", default="0.4",
                                     help="Guide hilight opacity")
        self.OptionParser.add_option("--nv_guides_around_page",
                                     action="store", type="inkbool",
                                     dest="nv_guides_around_page", default="False",
                                     help="Guides around page")
        # Test
        self.OptionParser.add_option("--nv_force_reload",
                                     action="store", type="inkbool",
                                     dest="nv_force_reload", default=True,
                                     help="Force document reload")
        self.OptionParser.add_option("--nv_testguides_u",
                                     action="store", type="string",
                                     dest="nv_testguides_u", default="cm",
                                     help="Test Guides Unit")
        self.OptionParser.add_option("--nv_testguides_x",
                                     action="store", type="float",
                                     dest="nv_testguides_x", default=2.5,
                                     help="Test Guides X")
        self.OptionParser.add_option("--nv_testguides_y",
                                     action="store", type="float",
                                     dest="nv_testguides_y", default=4.5,
                                     help="Test Guides Y")
        self.OptionParser.add_option("--nv_testguides_w",
                                     action="store", type="float",
                                     dest="nv_testguides_w", default=5.0,
                                     help="Test Guides Width")
        self.OptionParser.add_option("--nv_testguides_h",
                                     action="store", type="float",
                                     dest="nv_testguides_h", default=10.0,
                                     help="Test Guides Height")
        self.OptionParser.add_option("--nv_testguides_rect",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect", default=True,
                                     help="Test Guides around rect")
        self.OptionParser.add_option("--nv_testguides_rect_object",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_rect_object", default=True,
                                     help="Test Draw rectangle")
        self.OptionParser.add_option("--nv_testguides_hor",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_hor", default=True,
                                     help="Test horizontal guide")
        self.OptionParser.add_option("--nv_testguides_ver",
                                     action="store", type="inkbool",
                                     dest="nv_testguides_ver", default=True,
                                     help="Test vertical guide")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")
        self.OptionParser.add_option("--notebook_page_size",
                                     action="store", type="string",
                                     dest="notebook_page_size")
        self.OptionParser.add_option("--notebook_user_units",
                                     action="store", type="string",
                                     dest="notebook_user_units")
        self.OptionParser.add_option("--notebook_guides_test",
                                     action="store", type="string",
                                     dest="notebook_guides_test")
        # scope
        self.OptionParser.add_option("--nv_use_dpi",
                                     action="store", type="string",
                                     dest="nv_use_dpi", default="96",
                                     help="Use internal resolution")
        self.OptionParser.add_option("--nv_use_guides_units",
                                     action="store", type="string",
                                     dest="nv_use_guides_units", default="96",
                                     help="Use internal resolution")
        self.OptionParser.add_option("--nv_set_all",
                                     action="store", type="inkbool",
                                     dest="nv_set_all", default=False,
                                     help="Apply in all tabs")
        self.OptionParser.add_option("--nv_verbose",
                                     action="store", type="inkbool",
                                     dest="nv_verbose", default=False,
                                     help="Verbose mode")
        self.OptionParser.add_option("--nv_query_only",
                                     action="store", type="inkbool",
                                     dest="nv_query_only", default=False,
                                     help="Query only (don't modify)")

    def set_attributes_size(self, node):

        def _get_page_options():
            page_w = self.options.page_width
            page_h = self.options.page_height
            page_u = self.options.nv_page_unit_id
            if self.options.notebook_page_size == '"tab_page_size_presets"':
                pagesize = get_page_size_from_format(
                    self.options.nv_page_format, self.options.nv_page_orientation)
                if pagesize:
                    page_w, page_h, page_u = pagesize
            return (page_w, page_h, page_u)

        def _get_viewbox_factor_from_preset(page_u):
            if self.options.svg_viewbox_scale == "auto":
                if self.rt.get('viewBox'):
                    vb_factor = 1.0
                else:
                    vb_factor = self.uuconv[page_u] / self.uuconv['px']
            else:
                vb_factor = self.uuconv[page_u] / self.uuconv[self.options.svg_viewbox_scale]
            return vb_factor

        type_ = None
        params = []

        page_w, page_h, page_u = _get_page_options()
        page_f = self.options.svg_page_scale_factor

        if page_f <= 0.0:
            inkex.errormsg("Page scale factor needs to be > 0. Falling back to 1.0")
            page_f = 1.0

        if self.options.notebook_user_units == '"tab_user_units_presets"':
            vb_f = _get_viewbox_factor_from_preset(page_u)
            type_ = 'whuf'
            params = [page_w, page_h, page_u, vb_f * page_f]
        elif self.options.notebook_user_units == '"tab_user_units_custom"':
            vb_f_x = self.options.svg_viewbox_scale_custom_x
            if self.options.svg_viewbox_scale_custom_uniform:
                vb_f_y = vb_f_x
            else:
                vb_f_y = self.options.svg_viewbox_scale_custom_y
            type_ = 'wufhuf'
            params = [page_w, page_u, vb_f_x * page_f, page_h, page_u, vb_f_y * page_f]
        elif self.options.notebook_user_units == '"tab_user_units_viewbox"':
            vb_str = self.options.svg_viewbox_custom_string
            vb_ratio = "%s %s" % (self.options.svg_viewbox_custom_aspect_align,
                                  self.options.svg_viewbox_custom_aspect_clip)
            type_ = 'whufvbr'
            params = [page_w, page_h, page_u, page_f, vb_str, vb_ratio]
        else:
            type_ = 'whuf'
            params = [page_w, page_h, page_u, page_f]

        self.set_display_units(self.options.nv_display_unit_id)
        self.set_svg_size_with(type_, self.rt, params)

        if self.options.svg_viewbox_custom_rect == "rect":
            self.create_viewbox_rect()
        elif self.options.svg_viewbox_custom_rect == "clip":
            self.clip_layer_to_viewBox()
        else:
            pass

    def set_attributes_test(self, node):

        def _force_document_reload():
            self.rt.set('id', self.uniqueId("SVGRoot_"))

        def _create_layer(label):
            layer = inkex.etree.SubElement(self.rt, 'g')
            layer.set(inkex.addNS('label', 'inkscape'), label)
            layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
            return layer

        def _create_guides_from_preset():
            rect1 = (2.5, 4.5, 5.0, 10.0, 'cm')
            rect2 = (4.0, 1.5, 2.0, 3.0, 'in')
            rect3 = (650.0, 20.0, 100.0, 50.0, 'px')
            if self.options.nv_testguides_rect_object:
                parent = _create_layer('Guide test')
            for rect in [rect1, rect2, rect3]:
                if self.options.nv_testguides_rect:
                    self.create_guides_around_rect(*rect)
                if self.options.nv_testguides_rect_object:
                    self.create_test_rect(parent, *rect)
                if self.options.nv_testguides_hor:
                    y = self._unit2unit(rect[1], rect[4], self.page_unit)
                    self.create_horizontal_guideline("horizontal", y)
                if self.options.nv_testguides_ver:
                    x = self._unit2unit(rect[0], rect[4], self.page_unit)
                    self.create_vertical_guideline("vertical", x)

        def _create_guides_from_custom():
            x, y = (self.options.nv_testguides_x, self.options.nv_testguides_y)
            w, h = (self.options.nv_testguides_w, self.options.nv_testguides_h)
            u = self.options.nv_testguides_u
            if u == "display":
                u = node.get(inkex.addNS('document-units', 'inkscape'))
            if not u:
                u = "px"
            if self.options.nv_testguides_rect:
                self.create_guides_around_rect(x, y, w, h, u)
            if self.options.nv_testguides_rect_object:
                parent = _create_layer('Guide test')
                self.create_test_rect(parent, x, y, w, h, u)
            if self.options.nv_testguides_hor:
                y = self._unit2unit(y, u, self.page_unit)
                self.create_horizontal_guideline("horizontal", y)
            if self.options.nv_testguides_ver:
                x = self._unit2unit(x, u, self.page_unit)
                self.create_vertical_guideline("vertical", x)

        if self.options.notebook_guides_test == '"tab_nv_testguides_reload"':
            _force_document_reload()
        else:
            if self.options.nv_guides_around_page:
                self.create_guides_around_page()
            if self.options.notebook_guides_test == '"tab_nv_testguides_presets"':
                _create_guides_from_preset()
            elif self.options.notebook_guides_test == '"tab_nv_testguides_custom"':
                _create_guides_from_custom()
            else:  # unknown tab
                pass
            node.set('showguides', str(self.options.nv_showguides))

    def query_attributes(self, node):
        # unpack globals
        page_width, page_width_unit, page_height, page_height_unit = self.page_size
        vb_width, vb_height = self.view_box[2:4]
        x_scale, y_scale = self.aspect_ratio[2:4]
        # inkscape version (runtime)
        inkex.debug("Inkscape version:\t%s\n"
                    % self.ink_vers)
        # globals: SVG user units
        inkex.debug("SVG user unit (x-scale, in page unit): \t%s (%s)"
                    % (formatted(x_scale), page_width_unit))
        inkex.debug("SVG user unit (y-scale, in page unit): \t%s (%s)"
                    % (formatted(y_scale), page_height_unit))
        inkex.debug("SVG user unit (x-scale, in CSS Pixel): \t%s (px)"
                    % formatted(x_scale * self.uuconv[page_width_unit]))
        inkex.debug("SVG user unit (y-scale, in CSS Pixel): \t%s (px)\n"
                    % formatted(y_scale * self.uuconv[page_height_unit]))
        # globals: page size
        page_unit = self.page_unit
        inkex.debug("Page unit:   \t%s"
                    % page_unit)
        inkex.debug("Page width:  \t%s"
                    % formatted(page_width))
        inkex.debug("Page height: \t%s\n"
                    % formatted(page_height))
        # globals: viewBox
        inkex.debug("SVG viewBox width:   \t%s"
                    % formatted(vb_width))
        inkex.debug("SVG viewBox height:  \t%s\n"
                    % formatted(vb_height))
        inkex.debug("preserveAspectRatio: \t%s\n"
                    % self.rt.get('preserveAspectRatio', "xMidYMid meet"))
        # globals: scale factor
        inkex.debug("Document scale x:  \t1 : %s"
                    % formatted(1.0 / x_scale))
        inkex.debug("Document scale y:  \t1 : %s\n"
                    % formatted(1.0 / y_scale))
        # document display units
        inkex.debug("Document Display unit: %s\n"
                    % self.nv.get(inkex.addNS('document-units', 'inkscape')))
        # inkex
        doc_unit = self.getDocumentUnit()
        doc_width, doc_height = (self.unittouu(self.rt.get(dim)) for dim in ['width', 'height'])
        inkex.debug("Document unit (inkex):   \t%s"
                    % doc_unit)
        inkex.debug("Document width (inkex):  \t%s"
                    % formatted(doc_width))
        inkex.debug("Document height (inkex): \t%s\n"
                    % formatted(doc_height))

    def set_attributes_all(self, node):
        self.set_attributes_size(node)
        self.set_attributes_test(node)

    def doit(self, node):
        if self.options.nv_query_only:
            self.query_attributes(node)
        elif self.options.nv_set_all:
            self.set_attributes_all(node)
        elif self.options.notebook_main == '"tab_size"':
            self.set_attributes_size(node)
        elif self.options.notebook_main == '"tab_test"':
            self.set_attributes_test(node)
        else:
            inkex.debug("active tab: %s" % self.options.tab)
            inkex.debug("This tab does not apply an effect.")

    def use_myTemplate(self):
        # support 90 and 96 dpi
        if self.options.nv_use_dpi == "90":
            self.uuconv = UUCONV_90DPI  # inkex.uuconv (0.48, 0.91pre)
        elif self.options.nv_use_dpi == "96":
            self.uuconv = UUCONV_96DPI  # inkex.Effect._Effect__uuconv (0.91+)
        else:
            try:
                self.uuconv = inkex.Effect._Effect__uuconv
            except AttributeError:
                self.uuconv = inkex.uuconv
        # override version to store guides in User units:
        if self.options.nv_use_guides_units == "uu":
            self.ink_vers = "0.91+devel"
        # do things
        self.doit(self.nv)


if __name__ == '__main__':
    effect = C()
    effect.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
