#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
empty_color.py
Create empty document with custom page color

Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# standard library
# local library
import inkex


class C(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.rt = None
        self.nv = None

        # Color
        self.OptionParser.add_option("--nv_page_color",
                                     action="store", type="int",
                                     dest="nv_page_color", default=-256,
                                     help="Custom background color")
        # Color as string
        self.OptionParser.add_option("--nv_page_color_string",
                                     action="store", type="string",
                                     dest="nv_page_color_string", default="#0000ff",
                                     help="Page color")
        self.OptionParser.add_option("--nv_page_opacity_float",
                                     action="store", type="float",
                                     dest="nv_page_opacity_float", default="0.4",
                                     help="Page opacity")
        # tabs
        self.OptionParser.add_option("--notebook_main",
                                     action="store", type="string",
                                     dest="notebook_main")

    # helper

    def get_color_from_int(self, i):
        r = ((i >> 24) & 255)
        g = ((i >> 16) & 255)
        b = ((i >> 8) & 255)
        c = "#%02x%02x%02x" % (r, g, b)
        o = (((i) & 255) / 255.0)
        return (c, o)

    # methods

    def set_pagecolor(self, node, color, opacity):
        node.set('pagecolor', color)
        node.set(inkex.addNS('pageopacity', 'inkscape'), str(round(opacity, 3)))

    def set_attributes_page_color_from_string(self, node):
        """
        Set color from string input
        """
        c = self.options.nv_page_color_string
        o = self.options.nv_page_opacity_float
        self.set_pagecolor(node, c, o)

    def set_attributes_page_color(self, node):
        """
        Use the color widget
        """
        c, o = self.get_color_from_int(self.options.nv_page_color)
        self.set_pagecolor(node, c, o)

    def doit(self, node):
        if self.options.notebook_main == '"tab_page_color"':
            self.set_attributes_page_color(node)
        elif self.options.notebook_main == '"tab_page_color_string"':
            self.set_attributes_page_color_from_string(node)
        else:
            inkex.debug("active tab: %s" % self.options.notebook_main)

    # main function

    def effect(self):
        nv = self.getNamedView()
        self.doit(nv)


if __name__ == '__main__':
    effect = C()
    effect.affect()

# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
