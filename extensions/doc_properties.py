#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
doc_properties.py
Module for querying and modifying document properties

Based on original work by Jan Darowski and Tavmjong Bah.
Combined into shared module by ~suv.

Copyright (C) 2013 Jan Darowski
Copyright (C) 2014 Tavmjong Bah <tavmjong@free.fr>
Copyright (C) 2014 ~suv <suv-sf@users.sf.net>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
"""
# pylint: disable=invalid-name
# pylint: disable=missing-docstring
# pylint: disable=bad-whitespace
# pylint: disable=fixme
# pylint: disable=broad-except
# pylint: disable=no-member

# import
import inkex
import re

# globals

DEBUG = 1

UUCONV_90DPI = {'in':90.0,
                'pt':1.25,
                'px':1,
                'mm':3.5433070866,
                'cm':35.433070866,
                'm':3543.3070866,
                'km':3543307.0866,
                'pc':15.0,
                'yd':3240,
                'ft':1080}

UUCONV_96DPI = {'in':96.0,
                'pt':1.33333333333,
                'px':1.0,
                'mm':3.77952755913,
                'cm':37.7952755913,
                'm':3779.52755913,
                'km':3779527.55913,
                'pc':16.0,
                'yd':3456.0,
                'ft':1152.0}

INKSCAPE_PAPERSIZES = {'default'                 : (   210,   297, "mm"),
                       'A4'                      : (   210,   297, "mm"),
                       'US Letter'               : (   8.5,    11, "in"),
                       'US Legal'                : (   8.5,    14, "in"),
                       'US Executive'            : (  7.25,  10.5, "in"),
                       'A0'                      : (   841,  1189, "mm"),
                       'A1'                      : (   594,   841, "mm"),
                       'A2'                      : (   420,   594, "mm"),
                       'A3'                      : (   297,   420, "mm"),
                       'A5'                      : (   148,   210, "mm"),
                       'A6'                      : (   105,   148, "mm"),
                       'A7'                      : (    74,   105, "mm"),
                       'A8'                      : (    52,    74, "mm"),
                       'A9'                      : (    37,    52, "mm"),
                       'A10'                     : (    26,    37, "mm"),
                       'B0'                      : (  1000,  1414, "mm"),
                       'B1'                      : (   707,  1000, "mm"),
                       'B2'                      : (   500,   707, "mm"),
                       'B3'                      : (   353,   500, "mm"),
                       'B4'                      : (   250,   353, "mm"),
                       'B5'                      : (   176,   250, "mm"),
                       'B6'                      : (   125,   176, "mm"),
                       'B7'                      : (    88,   125, "mm"),
                       'B8'                      : (    62,    88, "mm"),
                       'B9'                      : (    44,    62, "mm"),
                       'B10'                     : (    31,    44, "mm"),
                       'C0'                      : (   917,  1297, "mm"),
                       'C1'                      : (   648,   917, "mm"),
                       'C2'                      : (   458,   648, "mm"),
                       'C3'                      : (   324,   458, "mm"),
                       'C4'                      : (   229,   324, "mm"),
                       'C5'                      : (   162,   229, "mm"),
                       'C6'                      : (   114,   162, "mm"),
                       'C7'                      : (    81,   114, "mm"),
                       'C8'                      : (    57,    81, "mm"),
                       'C9'                      : (    40,    57, "mm"),
                       'C10'                     : (    28,    40, "mm"),
                       'D1'                      : (   545,   771, "mm"),
                       'D2'                      : (   385,   545, "mm"),
                       'D3'                      : (   272,   385, "mm"),
                       'D4'                      : (   192,   272, "mm"),
                       'D5'                      : (   136,   192, "mm"),
                       'D6'                      : (    96,   136, "mm"),
                       'D7'                      : (    68,    96, "mm"),
                       'E3'                      : (   400,   560, "mm"),
                       'E4'                      : (   280,   400, "mm"),
                       'E5'                      : (   200,   280, "mm"),
                       'E6'                      : (   140,   200, "mm"),
                       'CSE'                     : (   462,   649, "pt"),
                       'US #10 Envelope'         : ( 4.125,   9.5, "in"),
                       'DL Envelope'             : (   110,   220, "mm"),
                       'Ledger/Tabloid'          : (    11,    17, "in"),
                       'Banner 468x60'           : (    60,   468, "px"),
                       'Icon 16x16'              : (    16,    16, "px"),
                       'Icon 32x32'              : (    32,    32, "px"),
                       'Icon 48x48'              : (    48,    48, "px"),
                       'Business Card (ISO 7810)': ( 53.98, 85.60, "mm"),
                       'Business Card (US)'      : (     2,   3.5, "in"),
                       'Business Card (Europe)'  : (    55,    85, "mm"),
                       'Business Card (Aus/NZ)'  : (    55,    90, "mm"),
                       'Arch A'                  : (     9,    12, "in"),
                       'Arch B'                  : (    12,    18, "in"),
                       'Arch C'                  : (    18,    24, "in"),
                       'Arch D'                  : (    24,    36, "in"),
                       'Arch E'                  : (    36,    48, "in"),
                       'Arch E1'                 : (    30,    42, "in")}

BACKGROUND_PRESETS = {'default'              : ("#ffffff", "#666666", 0.0, "true"),
                      'white_transp'         : ("#ffffff", "#666666", 0.0, "false"),
                      'white_opaque'         : ("#ffffff", "#666666", 1.0, "false"),
                      'gray_transp'          : ("#808080", "#444444", 0.0, "false"),
                      'gray_opaque'          : ("#808080", "#444444", 1.0, "false"),
                      'black_transp'         : ("#000000", "#999999", 0.0, "false"),
                      'black_opaque'         : ("#000000", "#999999", 1.0, "false")}


def get_format(pf, po):
    return get_page_size_from_format(pf, po)


def get_page_size_from_format(page_format, page_orientation):
    try:
        known_papersize = INKSCAPE_PAPERSIZES[page_format]
    except KeyError, error_msg:
        inkex.debug("Unknpwn paper format, using fallback A4. (Error: %s)." % error_msg)
        known_papersize = INKSCAPE_PAPERSIZES['default']
    unit = known_papersize[2]
    if page_orientation == "portrait":
        width = float(known_papersize[0])
        height = float(known_papersize[1])
    else:
        width = float(known_papersize[1])
        height = float(known_papersize[0])
    return (width, height, unit)


def formatted(f):
    return format(f, '.8f').rstrip('0').rstrip('.')


def inkbool2string(inkbool):
    if inkbool:
        return "true"
    else:
        return "false"


def get_color_from_int(i):
    r = ((i >> 24) & 255)
    g = ((i >> 16) & 255)
    b = ((i >> 8) & 255)
    # c = "rgb(%s,%s,%s)" % (r, g, b)
    c = "#%02x%02x%02x" % (r, g, b)
    o = (((i) & 255) / 255.0)
    return (c, o)


def is_layer(node):
    return (node.tag == inkex.addNS('g', 'svg') and
            node.attrib.get(inkex.addNS('groupmode', 'inkscape'), '').lower() == 'layer')


def remove_sublayers(node):
    for item in node.iterchildren():
        if is_layer(item):
            item.getparent().remove(item)


# class
class DocProperties(inkex.Effect):
    def __init__(self):
        inkex.Effect.__init__(self)
        self.uuconv = None
        self.rt = None
        self.nv = None
        self.ink_vers = None
        self.page_size = []
        self.view_box = []
        self.aspect_ratio = []
        # legacy
        self.page_unit = None

    # TODO

    # def add_layers(self, node, layer_name, count):
    #     for i in range(int(count)):
    #         # FIXME: sanitze layer_name for id
    #         li = "%s%s" % (layer_name.lower(), i+1)
    #         ln = "%s %s" % (layer_name, i+1)
    #         lg = inkex.etree.SubElement(node.getroot(), inkex.addNS('g', 'svg'))
    #         lg.set('id', li)
    #         lg.set(inkex.addNS('label', 'inkscape'), ln)
    #         lg.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
    #     return li

    # helper: misc

    def _get_inkscape_version(self):
        # NOTE: r14965 changed how this attribute is updated:
        # Inkscape no longer updates the attribute on load but on save.
        # New files thus don't have such an attribute anymore.
        # quick workaround: default to (assumed) 0.92
        inkscape_version_string = self.rt.get(inkex.addNS('version', 'inkscape'), "0.92")
        version_match = re.compile(r'(0.48|0.48\+devel|0.91pre|0.91\+devel|0.91|0.92)')
        match_result = version_match.search(inkscape_version_string)
        if match_result is not None:
            inkscape_version = match_result.groups()[0]
        else:
            inkex.errormsg("Failed to retrieve Inkscape version.\n")
            inkscape_version = "Unknown"
        return inkscape_version

    # helper: unit conversion between real-world units (absolute)

    def _are_near_relative(self, a, b, eps):
        a, b = [abs(x) for x in [a, b]]
        if (a-b <= a*eps) and (a-b >= -a*eps):
            return True
        else:
            inkex.debug('{} <= {}: {}'.format(a - b, a * eps, a - b <= a * eps))
            inkex.debug('{} >= {}: {}'.format(a - b, -a * eps, a - b >= -a * eps))
            return False

    def _check_unit_conversion_match(self, r1, r2, eps=1e-9, verbose=False):
        def print_(result):
            inkex.debug('inkex, local unit conversions {} (tolerance: {}):'.format(result, eps))
            inkex.debug('r1 - r2: {}\nr2 - r1: {}'.format(r1 - r2, r2 - r1))
            inkex.debug('inkex: {}\nlocal: {}'.format(r1, r2))

        if not self._are_near_relative(r1, r2, eps):
            print_('do not match')
        elif verbose:
            print_('match')

    def _unittopx(self, string):
        '''Returns CSS Pixel quantity given a *string* representation of units in another system'''
        unit = re.compile('(%s)$' % '|'.join(self.uuconv.keys()))
        value = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        p = value.match(string)
        u = unit.search(string)
        if p:
            retval = float(p.string[p.start():p.end()])
        else:
            retval = 0.0
        if u:
            try:
                return retval * self.uuconv[u.string[u.start():u.end()]]
            except KeyError:
                pass
        return retval

    def _pxtounit(self, value, unit):
        """Returns quantity in *unit* of given *value* in CSS Pixel"""
        return value / self.uuconv[unit]

    def _unit2csspx(self, q, u):
        return q * self.uuconv[u]

    def _csspx2unit(self, q, u):
        return q / self.uuconv[u]

    def _unit2unit(self, q, u1, u2):
        # # return self.uutounit(self.unittouu("%s%s" % (q, u1)), u2)
        # # return self._pxtounit(self._unittopx("%s%s" % (q, u1)), u2)
        # # return self._csspx2unit(self._unit2csspx(q, u1), u2)
        # return (q * self.uuconv[u1]) / self.uuconv[u2]
        inkex_result = self.uutounit(self.unittouu("%s%s" % (q, u1)), u2)
        local_result = (q * self.uuconv[u1]) / self.uuconv[u2]
        self._check_unit_conversion_match(inkex_result, local_result)
        return local_result

    def _get_px(self, q, u):
        return self._unit2unit(q, u, 'px')

    def _get_inverted_y(self, y, u):
        """gets height in page unit, returns y offset in specified unit"""
        # works for new layer and in root - what about preserved transforms on parent containers?
        return self._unit2unit(self.page_size[2], self.page_size[3], u) - y

    def _get_offset_x(self, q, u):
        """gets viewBox origin x from _get_aspect() in page units,
           returns offset in specified unit"""
        return q - self._unit2unit(self.aspect_ratio[0], self.page_size[1], u)

    def _get_offset_y(self, q, u):
        """gets viewBox origin y from _get_aspect() in page units,
           returns offset in specified unit"""
        return q - self._unit2unit(self.aspect_ratio[1], self.page_size[3], u)

    # helper: unit relations for SVG user units to page unit

    # aspect_align == none

    def _get_uu_x_raw(self, q, u):
        """returns quantity in user units based on page width / viewBox width ratio"""
        # use for guides in current trunk and patched 0.91pre3
        return self._unit2unit(q, u, self.page_size[1]) / (self.page_size[0] / self.view_box[2])

    def _get_uu_y_raw(self, q, u):
        """returns quantity in user units based on page height / viewBox height ratio"""
        # use for guides in current trunk and patched 0.91pre3"""
        return self._unit2unit(q, u, self.page_size[3]) / (self.page_size[2] / self.view_box[3])

    def _get_uu_rect_raw(self, params, u):
        x, y, w, h = params
        # offset (inverted desktop coords)
        y = self._get_inverted_y(y + h, u)
        # convert to user units
        x, y = (self._get_uu_x_raw(x, u), self._get_uu_y_raw(y, u))
        w, h = (self._get_uu_x_raw(w, u), self._get_uu_y_raw(h, u))
        # add viewBox x, y (in user units)
        x += self.view_box[0]
        y += self.view_box[1]
        return (x, y, w, h)

    # aspect_align != none

    def _get_uu_x(self, q, u):
        """returns quantity in user units, adjusted by x-scale based on preserveAspectRatio"""
        return self._unit2unit(q, u, self.page_size[1]) / self.aspect_ratio[2]

    def _get_uu_y(self, q, u):
        """returns quantity in user units, adjusted by y-scale based on preserveAspectRatio"""
        return self._unit2unit(q, u, self.page_size[3]) / self.aspect_ratio[3]

    def _get_uu_rect(self, params, u):
        x, y, w, h = params
        # offset (inverted desktop coords)
        y = self._get_inverted_y(y + h, u)
        # offset (viewBox, preserveAspectRatio, based on page unit, returned in specified units)
        x = self._get_offset_x(x, u)
        y = self._get_offset_y(y, u)
        # convert to user units
        x, y = (self._get_uu_x(x, u), self._get_uu_y(y, u))
        w, h = (self._get_uu_x(w, u), self._get_uu_y(h, u))
        # add viewBox x, y (in user units)
        x += self.view_box[0]
        y += self.view_box[1]
        return (x, y, w, h)

    # helper: store current document size, unit and viewBox

    def _update_globals(self):
        self.aspect_ratio = list(self._get_aspect())
        # legacy
        self.page_unit = self.page_size[1]

    def _update_page_size(self, params, update_static=False):
        w, wu, h, hu = params
        self.page_size[0:2] = [float(w), wu]
        self.page_size[2:4] = [float(h), hu]
        if update_static:
            self._update_globals()

    def _update_view_box(self, params, update_static=False):
        x, y, w, h = params
        self.view_box[0:2] = [float(x), float(y)]
        self.view_box[2:4] = [float(w), float(h)]
        if update_static:
            self._update_globals()

    def _update_page_size_and_view_box(self, page, viewbox, update_static=True):
        self._update_page_size(page)
        self._update_view_box(viewbox)
        if update_static:
            self._update_globals()

    # helper: get scale factors for current document

    def _get_aspect(self):
        """returns list of 2 lists: 'offset' [x, y] and 'scale' [scale_x, scale_y] """
        w, wu, h, hu = self.page_size
        vbx, vby, vbw, vbh = self.view_box
        x = y = 0.0
        width, height = [w, h]
        scale = 1.0
        aspect_align, aspect_clip = self.rt.get('preserveAspectRatio', "xMidYMid meet").split()
        if aspect_align != "none":
            scale_x = w / vbw
            scale_y = h / vbh
            scale = aspect_clip == "meet" and min(scale_x, scale_y) or max(scale_x, scale_y)
            width = vbw * scale
            height = vbh * scale
            align_x = aspect_align[1:4]
            align_y = aspect_align[5:8]
            offset_factor = {'Min': 0.0, 'Mid': 0.5, 'Max': 1.0}
            x = offset_factor[align_x] * (w - width)
            y = offset_factor[align_y] * (h - height)
        return (x, y, width / vbw, height / vbh)

    # helper: page size, units, viewBox, window

    def _get_attributes_size(self):
        width = self.rt.get('width')
        height = self.rt.get('height')
        value_match = re.compile(r'(([-+]?[0-9]+(\.[0-9]*)?|[-+]?\.[0-9]+)([eE][-+]?[0-9]+)?)')
        unit_match = re.compile('(%s)$' % '|'.join(self.uuconv.keys()))
        wv = hv = "100"
        wu = hu = "px"
        # mwv, mhv = (value_match.match(val).group() for val in [width, height])
        mwv = value_match.match(width)
        if mwv:
            wv = mwv.group()
        mhv = value_match.match(height)
        if mhv:
            hv = mhv.group()
        # mwu, mhu = (unit_match.search(val).group() for val in [width, height])
        mwu = unit_match.search(width)
        if mwu:
            wu = mwu.group()
        mhu = unit_match.search(height)
        if mhu:
            hu = mhu.group()
        return (wv, wu, hv, hu)

    def _get_pagesize(self):
        wv, wu, hv, hu = self._get_attributes_size()
        # fallback (missing unit identifier)
        if not wu:
            wu = "px"
        if not hu:
            hu = "px"
        # FIXME: relative units: %
        if wu == "%":
            wu = "mm"
            wv = 210.0 * (float(wv) / 100.0)
        if hu == "%":
            hu = "mm"
            hv = 297.0 * (float(hv) / 100.0)
        # FIXME: not yet supported: em
        if wu == "em":
            wu = "px"
            wv = "100"
        if hu == "em":
            hu = "px"
            hv = "100"
        return (float(wv), wu, float(hv), hu)

    def _get_viewbox(self):
        w, wu, h, hu = self.page_size
        vbx = vby = 0.0
        vbw = float(self._unit2csspx(w, wu))
        vbh = float(self._unit2csspx(h, hu))
        vbstr = self.rt.get('viewBox', None)
        if vbstr is not None:
            try:
                vbx, vby, vbw, vbh = (float(i) for i in vbstr.split())
            except ValueError, error_msg:
                if DEBUG:
                    inkex.errormsg("Failed to parse viewBox attribute ('%s'): %s"
                                   % (vbstr, error_msg))
                    inkex.errormsg("Fallback values for viewBox are:")
                    inkex.errormsg("vbx: %f, vby: %f, vbw: %f, vbh: %f"
                                   % (vbx, vby, vbw, vbh))
                else:
                    pass
        return (vbx, vby, vbw, vbh)

    def _get_window_dimension(self, dim):
        try:
            return float(self.nv.get(inkex.addNS("window-%s" % dim, 'inkscape')))
        except TypeError:
            return 512.0

    def _get_zoom_min(self, area, window_size=None):
        window_chrome = 200.0  # empirical (works ok with Adwaita GTK2)
        if window_size is None:
            window_width = self._get_window_dimension("width")
            window_height = self._get_window_dimension("height")
        else:
            try:
                window_width, window_height = [float(v) for v in window_size[0:2]]
            except TypeError:
                window_width = window_height = 512.0
        w, wu, h, hu = area
        zoomw = (window_width - window_chrome) / self._get_px(w, wu)
        zoomh = (window_height - window_chrome) / self._get_px(h, hu)
        return min(zoomw, zoomh)

    # methods: page size, units, window

    def _set_svg_size(self, node, page, viewbox, ratio=None):
        node.set('id', "SVGRoot")
        node.set('width', "%s%s" % (formatted(page[0]), page[1]))
        node.set('height', "%s%s" % (formatted(page[2]), page[3]))
        node.set('viewBox', "%s %s %s %s" % tuple(formatted(v) for v in viewbox))
        if ratio is not None:
            node.set('preserveAspectRatio', ratio)
        elif 'preserveAspectRatio' in node.attrib:
            del node.attrib['preserveAspectRatio']
        self._update_page_size_and_view_box(page, viewbox)
        # inkex.debug(self._get_aspect())

    def _set_svg_size_page(self, node, params):
        # page without scaling
        w, h, u = params[0:3]
        self._set_svg_size(node, [w, u, h, u], [0.0, 0.0, w, h])

    def _set_svg_size_page_scaled(self, node, params):
        # page with isotropic scaling
        w, h, u, f = params
        self._set_svg_size(node, [w, u, h, u], [0.0, 0.0, w * f, h * f])

    def _set_svg_size_page_scaled_non_isotrop(self, node, params):
        # page with non-isotropic scaling
        w, wu, fx, h, hu, fy = params
        self._set_svg_size(node, [w, wu, h, hu], [0.0, 0.0, w * fx, h * fy])

    def _set_svg_size_page_scaled_viewbox(self, node, params):
        # page with isotropic scaling and arbitrary viewBox, aspectRatio
        w, h, u, f, vb, r = params
        try:
            vbx, vby, vbw, vbh = [float(i) * f for i in vb.split()]
        except ValueError, error_msg:
            inkex.errormsg("Invalid viewBox '%s' string detected (Error: %s)."
                           % (vb, error_msg))
            inkex.errormsg("Falling back to viewBox based on page width and height.")
            vbx, vby, vbw, vbh = [0.0, 0.0, w * f, h * f]
        self._set_svg_size(node, [w, u, h, u], [vbx, vby, vbw, vbh], r)

    def _set_svg_size_unknown(self, node, params):
        inkex.errormsg("Unknown type of size parameters - unable to set new SVG size.")
        inkex.debug(params)
        # sys.exit(1)

    def set_svg_size_with(self, type_, node, params):
        """ dispatcher for setting SVG size based on *type_*"""
        size_function = getattr(self, '_set_svg_size_{}'.format(type_), self._set_svg_size_unknown)
        size_function(node, params)

    # methods: namedView attributes

    def set_display_units(self, u):
        self.nv.set(inkex.addNS('document-units', 'inkscape'), u)

    def set_units(self, u):
        self.nv.set('units', u)

    def set_window(self, wx, wy, ww, wh):
        self.nv.set(inkex.addNS('window-x', 'inkscape'), str(wx))
        self.nv.set(inkex.addNS('window-y', 'inkscape'), str(wy))
        self.nv.set(inkex.addNS('window-width', 'inkscape'), str(ww))
        self.nv.set(inkex.addNS('window-height', 'inkscape'), str(wh))

    def set_zoom_to_area(self, area, offset, zoom=None, window_size=None):
        if not zoom:  # if zoom is None or 0.0, calculate min zoom
            zoom = self._get_zoom_min(area, window_size)
        w, wu, h, hu = area
        origin_x, origin_y = offset
        center_x = (self.uuconv[wu] * (origin_x + w)) / 2.0
        center_y = (self.uuconv[hu] * (origin_y + h)) / 2.0
        self.nv.set(inkex.addNS('cx', 'inkscape'), str(int(center_x)))
        self.nv.set(inkex.addNS('cy', 'inkscape'), str(int(center_y)))
        self.nv.set(inkex.addNS('zoom', 'inkscape'), str(round(zoom, 3)))

    def set_zoom_to_page(self, zoom=None, window_size=None):
        page = list(self.page_size)
        self.set_zoom_to_area(page, [0.0, 0.0], zoom, window_size)

    # methods: layers

    def create_layer(self, label):
        layer = inkex.etree.SubElement(self.rt, 'g')
        layer.set('id', self.uniqueId("layer"))
        layer.set(inkex.addNS('label', 'inkscape'), label)
        layer.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
        return layer

    def set_current_layer(self, layer):
        """sets namedview attribute for current layer (layer needs to have 'id' attribute)"""
        if 'id' in layer.attrib:
            self.getNamedView().set(inkex.addNS('current-layer', 'inkscape'), layer.get('id'))

    def remove_layers(self, node):
        # TODO: check for groupmode attribute
        remove_sublayers(node)

    def add_layers(self, layer_name, count):
        parent = self.document.getroot()
        for i in range(int(count)):
            # FIXME: sanitize layer_name for id
            li = "%s%s" % (layer_name.lower(), i+1)
            ln = "%s %s" % (layer_name, i+1)
            lg = inkex.etree.SubElement(parent, inkex.addNS('g', 'svg'))
            lg.set('id', li)
            lg.set(inkex.addNS('label', 'inkscape'), ln)
            lg.set(inkex.addNS('groupmode', 'inkscape'), 'layer')
        return li

    # private methods: guide lines

    def _create_guideline(self, label, orientation, x, y):
        guide = inkex.etree.SubElement(self.nv, inkex.addNS('guide', 'sodipodi'))
        guide.set("orientation", orientation)
        guide.set("position", "%s,%s" % (formatted(x), formatted(y)))
        label = ""  # TODO: add switch for label vs coord vs none
        if label:
            guide.set(inkex.addNS('label', 'inkscape'), label)
        else:
            guide.set(inkex.addNS('label', 'inkscape'), " x=%s  y=%s "
                      % (formatted(x), formatted(y)))

    def _create_guides_around_rect_xy(self, x, y, w, h, wu, hu):
        # TODO: compare numerically to >= 0.92 for future releases
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            # guides in trunk use SVG user units (rev >= 13769)
            x0, y0 = (self._get_uu_x_raw(x, wu), self._get_uu_y_raw(y, hu))
            x1, y1 = (self._get_uu_x_raw(x + w, wu), self._get_uu_y_raw(y + h, hu))
        else:
            # guides in 0.91.x use CSS px
            x0, y0 = (self._get_px(x, wu), self._get_px(y, hu))
            x1, y1 = (self._get_px(x + w, wu), self._get_px(y + h, hu))
        self._create_guideline("bottom", "0,%s" % x1, x0, y0)
        self._create_guideline("right", "-%s,0" % y1, x1, y0)
        self._create_guideline("top", "0,-%s" % x1, x1, y1)
        self._create_guideline("left", "%s,0" % y1, x0, y1)

    # methods: guide lines

    def create_horizontal_guideline(self, name, position):
        # TODO: compare numerically to >= 0.92 for future releases
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self._get_uu_y_raw(position, self.page_size[3])
        else:
            position = self._get_px(position, self.page_size[3])
        self._create_guideline(name, "0,1", 0, position)

    def create_vertical_guideline(self, name, position):
        # TODO: compare numerically to >= 0.92 for future releases
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            position = self._get_uu_x_raw(position, self.page_size[1])
        else:
            position = self._get_px(position, self.page_size[1])
        self._create_guideline(name, "1,0", position, 0)

    def create_guides_around_rect(self, x, y, w, h, u):
        self._create_guides_around_rect_xy(x, y, w, h, u, u)

    def create_guides_around_page(self):
        w, wu, h, hu = self.page_size
        self._create_guides_around_rect_xy(0, 0, w, h, wu, hu)

    # private methods: grids

    def _create_grid(self, type_, unit, params):
        grid = inkex.etree.SubElement(self.nv, inkex.addNS('grid', 'inkscape'))
        grid.set('type', type_)
        grid.set('units', unit)
        if type_ == "xygrid":
            grid.set('originx', "%s" % params[0])
            grid.set('originy', "%s" % params[1])
            grid.set('spacingx', "%s" % params[2])
            grid.set('spacingy', "%s" % params[3])
        elif type_ == "axonomgrid":
            grid.set('originx', "%s" % params[0])
            grid.set('originy', "%s" % params[1])
            grid.set('spacingy', "%s" % params[3])
            grid.set('gridanglex', "%s" % params[4])
            grid.set('gridangley', "%s" % params[5])
        return grid

    # methods: grids

    def create_default_grid(self, type_):
        grid = inkex.etree.SubElement(self.nv, inkex.addNS('grid', 'inkscape'))
        grid.set('type', type_)
        return grid

    def create_grid_with_user_units(self, type_):
        grid = None
        params = []
        unit = self.page_unit
        if type_ == "xygrid":
            params = [0, 0, 1.0, 1.0, None, None]
            guid = self.uniqueId("1x1%s_rect" % unit)
        elif type_ == "axonomgrid":
            params = [0, 0, 0, 1.0, 30, 30]
            guid = self.uniqueId("1%s_axo" % unit)
        grid = self._create_grid(type_, unit, params)
        if grid is not None:
            grid.set('id', guid)
        return grid

    def create_page_grid(self, type_, unit, params):
        grid = None
        if unit == "display":
            if inkex.addNS('document-units', 'inkscape') in self.nv.attrib:
                unit = self.nv.get(inkex.addNS('document-units', 'inkscape'))
            else:
                unit = self.page_unit
        # TODO: compare numerically to >= 0.92 for future releases
        if self.ink_vers == "0.91+devel" or self.ink_vers == "0.92":
            # grids in trunk use SVG user units (rev >= 13769)
            params[0] = self._get_uu_x_raw(params[0], unit)
            params[1] = self._get_uu_y_raw(params[1], unit)
            params[2] = self._get_uu_x_raw(params[2], unit)
            params[3] = self._get_uu_y_raw(params[3], unit)
            # angle:
            # v[4] = v[4]
            # v[5] = v[5]
        else:
            # grids in 0.91.x use external units
            params[0] = "%s%s" % (params[0], unit)
            params[1] = "%s%s" % (params[1], unit)
            params[2] = "%s%s" % (params[2], unit)
            params[3] = "%s%s" % (params[3], unit)
            # angle:
            # v[4] = v[4]
            # v[5] = v[5]
        grid = self._create_grid(type_, unit, params)
        return grid

    def create_grid_with_display_units(self, type_):
        grid = None
        params = []
        if inkex.addNS('document-units', 'inkscape') in self.nv.attrib:
            unit = self.nv.get(inkex.addNS('document-units', 'inkscape'))
        else:
            inkex.errormsg("Failed to retrieve display units from document.")
            inkex.errormsg("Falling back to page unit.")
            unit = self.page_unit
        if type_ == "xygrid":
            params = [0, 0, 1.0, 1.0, None, None]
            guid = self.uniqueId("1x1%s_rect" % unit)
        elif type_ == "axonomgrid":
            params = [0, 0, 0, 1.0, 30, 30]
            guid = self.uniqueId("1%s_axo" % unit)
        grid = self.create_page_grid(type_, unit, params)
        if grid is not None:
            grid.set('id', guid)
        return grid

    def create_grid_with_document_units(self, type_):
        return self.create_grid_with_display_units(type_)

    # methods: page, canvas style

    def set_background(self, preset):
        try:
            bg = BACKGROUND_PRESETS[preset]
            self.nv.set('pagecolor', bg[0])
            self.nv.set('bordercolor', bg[1])
            self.nv.set(inkex.addNS('pageopacity', 'inkscape'), str(bg[2]))
            self.nv.set(inkex.addNS('showpageshadow', 'inkscape'), bg[3])
        except KeyError:
            pass

    def set_bordertype(self, type_):
        if type_ == "shadow":
            self.nv.set('showborder', "true")
            self.nv.set('borderlayer', "false")
            self.nv.set(inkex.addNS('showpageshadow', 'inkscape'), "true")
            self.nv.set(inkex.addNS('pageshadow', 'inkscape'), "2")
        elif type_ == "noshadow":
            self.nv.set('showborder', "true")
            self.nv.set('borderlayer', "true")
            self.nv.set(inkex.addNS('showpageshadow', 'inkscape'), "false")
        elif type_ == "guides":
            self.nv.set('showborder', "false")
            self.create_guides_around_page()

    # methods: create and modify objects

    def create_test_rect(self, node, x, y, w, h, u):
        x, y, w, h = self._get_uu_rect([x, y, w, h], u)
        rect_attribs = {'x': str(formatted(x)), 'y': str(formatted(y)),
                        'width': str(formatted(w)), 'height': str(formatted(h)),
                        'style': "opacity:0.25", }
        return inkex.etree.SubElement(node, inkex.addNS('rect', 'svg'), rect_attribs)

    def create_viewbox_rect(self, node=None):
        if node is None:
            node = self.rt
        vbx, vby, vbw, vbh = self.view_box
        rect_attribs = {'x': str(formatted(vbx)), 'y': str(formatted(vby)),
                        'width': str(formatted(vbw)), 'height': str(formatted(vbh)),
                        'style': "fill:blue;opacity:0.10", 'id': self.uniqueId("viewbox"), }
        return inkex.etree.SubElement(node, inkex.addNS('rect', 'svg'), rect_attribs)

    def clip_layer_to_viewBox(self, layer=None):
        if layer is None:
            layer = self.create_layer("viewBox")
        vb_rect = self.create_viewbox_rect()
        clip_id = self.uniqueId("viewBox_clip")
        clip_attribs = {'clipPathUnits': "userSpaceOnUse", 'id': clip_id, }
        defs = self.xpathSingle('/svg:svg//svg:defs')
        if defs is None:
            defs = inkex.etree.SubElement(self.document.getroot(), inkex.addNS('defs', 'svg'))
        vb_clip = inkex.etree.SubElement(defs, inkex.addNS('clipPath', 'svg'), clip_attribs)
        vb_clip.append(vb_rect)
        page_rect = self.create_test_rect(layer, 0, 0, self.page_size[0], self.page_size[2],
                                          self.page_size[1])
        page_rect.set('style', "fill:red;opacity:0.1")
        page_rect.set(inkex.addNS('insensitive', 'sodipodi'), "true")
        layer.set('clip-path', "url(#%s)" % clip_id)
        self.set_current_layer(layer)

    # finally: this makes the class effect() work for other template scripts

    def set_attribs(self):
        pass

    def effect(self):
        self.uuconv = inkex.Effect._Effect__uuconv
        # shorthand
        self.rt = self.document.getroot()
        self.nv = self.document.xpath('//sodipodi:namedview', namespaces=inkex.NSS)[0]
        # runtime inkscape version (for now, taken from SVGRoot)
        self.ink_vers = self._get_inkscape_version()
        # set global lists
        self.page_size = list(self._get_pagesize())
        self.view_box = list(self._get_viewbox())
        # set globals with scale factors
        self.aspect_ratio = list(self._get_aspect())
        # legacy
        self.page_unit = self.page_size[1]  # width Page unit
        # do it!
        self.set_attribs()


# vim: expandtab shiftwidth=4 tabstop=8 softtabstop=4 fileencoding=utf-8 textwidth=99
